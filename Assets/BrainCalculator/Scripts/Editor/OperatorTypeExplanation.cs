﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class OperatorTypeExplanation : EditorWindow {

    private Vector2 scrollPosition = Vector2.zero;


    [MenuItem("Tools/Operator Type Explanation")]
    public static void ShowWindow()
    {
        GetWindow(typeof(OperatorTypeExplanation));
    }


    private void OnGUI()
    {
        scrollPosition = GUILayout.BeginScrollView(scrollPosition, true, true, GUILayout.Width(Screen.width), GUILayout.Height(Screen.height));

        EditorGUILayout.BeginVertical("Box", GUILayout.Width(500));
        EditorGUILayout.LabelField("Operator Types Explanation");
        EditorGUILayout.LabelField(OperatorTypes.None + " : The none operator.");
        EditorGUILayout.LabelField(OperatorTypes.Plus + " : Plus the current number with the given number.");
        EditorGUILayout.LabelField(OperatorTypes.Subtract + " : Substract the current number to the given number.");
        EditorGUILayout.LabelField(OperatorTypes.Multiple + " : Multiple the current number with the given number.");
        EditorGUILayout.LabelField(OperatorTypes.Devide + " : Devide the current number for the given number.");
        EditorGUILayout.LabelField(OperatorTypes.Surplus + " : Devide the current number for the given number and return the surplus number.");
        EditorGUILayout.LabelField(OperatorTypes.DecreaseNum + " : Decrease all number in the string with the given value.");
        EditorGUILayout.LabelField(OperatorTypes.IncreaseNum + " : Increase all number in the string with the given value.");
        EditorGUILayout.LabelField(OperatorTypes.RemoveLastNum + " : Remove the last number of the current string.");
        EditorGUILayout.LabelField(OperatorTypes.RemoveFirstNum + " : Remove the first number of the current string.");
        EditorGUILayout.LabelField(OperatorTypes.InsertToTheLast + " : Insert a number to the last of the string.");
        EditorGUILayout.LabelField(OperatorTypes.InsertToTheFirst + " : Insert a number to the first of the string.");
        EditorGUILayout.LabelField(OperatorTypes.ConvertNums + " : Convert the number from x to y.");
        EditorGUILayout.LabelField(OperatorTypes.Duplicate + " : Duplicate the current number with a given number.");
        EditorGUILayout.LabelField(OperatorTypes.Opposite + " : Change the current number from positive/negative to negative/positive.");
        EditorGUILayout.LabelField(OperatorTypes.Reverse + " : Reverse the current number.");
        EditorGUILayout.LabelField(OperatorTypes.Sum + " : Sum all the single number of the string.");
        EditorGUILayout.LabelField(OperatorTypes.ShiftLeft + " : Shift the current number to the left.");
        EditorGUILayout.LabelField(OperatorTypes.ShiftRight + " : Shift the current number to the right.");
        EditorGUILayout.LabelField(OperatorTypes.Mirror + " : Mirror the current number.");
        EditorGUILayout.LabelField(OperatorTypes.RemoveNum + " : Remove a given number if existed in the string.");
        EditorGUILayout.LabelField(OperatorTypes.Portals + " : Remove Input number and add to out number");
        EditorGUILayout.LabelField(OperatorTypes.TenSubstractDigit + " : Make new Number by 10 minus each number");
        EditorGUILayout.LabelField(OperatorTypes.StoreResult + " : Store result into Store Button");
        EditorGUILayout.LabelField(OperatorTypes.ChangeValueButtonPlus2 + " : Change the value of button plus 2");
        EditorGUILayout.EndVertical();
        GUILayout.EndScrollView();
    }


}
