﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.IO;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class LevelEditor : EditorWindow
{

    private Vector2 scrollPosition = Vector2.zero;
    private int boxGroupSize = 350;
    private int height = 20;
    private float gridButtonSize = 100f;
    private int selectedOpeNumber = 1;

    private List<string> listOperator = new List<string>();
    private List<string> listHint = new List<string>();
    [MenuItem("Tools/ Level Editor")]
    public static void ShowWindow()
    {
        GetWindow(typeof(LevelEditor));
    }


    private void Awake()
    {
        LevelManager.LoadLevel(LevelManager.GetTotalLevel());

        //Add operator for list
        listOperator.Add(OperatorTypes.None);
        listOperator.Add(OperatorTypes.Plus);
        listOperator.Add(OperatorTypes.Subtract);
        listOperator.Add(OperatorTypes.Multiple);
        listOperator.Add(OperatorTypes.Devide);
        listOperator.Add(OperatorTypes.Surplus);
        listOperator.Add(OperatorTypes.IncreaseNum);
        listOperator.Add(OperatorTypes.DecreaseNum);
        listOperator.Add(OperatorTypes.RemoveLastNum);
        listOperator.Add(OperatorTypes.RemoveFirstNum);
        listOperator.Add(OperatorTypes.InsertToTheFirst);
        listOperator.Add(OperatorTypes.InsertToTheLast);
        listOperator.Add(OperatorTypes.ConvertNums);
        listOperator.Add(OperatorTypes.Duplicate);
        listOperator.Add(OperatorTypes.Opposite);
        listOperator.Add(OperatorTypes.Reverse);
        listOperator.Add(OperatorTypes.Sum);
        listOperator.Add(OperatorTypes.ShiftLeft);
        listOperator.Add(OperatorTypes.ShiftRight);
        listOperator.Add(OperatorTypes.Mirror);
        listOperator.Add(OperatorTypes.RemoveNum);
        listOperator.Add(OperatorTypes.Portals);
        listOperator.Add(OperatorTypes.TenSubstractDigit);
        listOperator.Add(OperatorTypes.StoreResult);
        //Add hints for the list
        listHint.Clear();
        for (int i = 0; i < LevelManager.levelData.listHintStep.Count; i++)
        {
            if (!listHint.Contains(LevelManager.levelData.listHintStep[i]))
                listHint.Add(LevelManager.levelData.listHintStep[i]);
        }

    }

    private void OnGUI()
    {
        scrollPosition = GUILayout.BeginScrollView(scrollPosition, true, true, GUILayout.Width(Screen.width), GUILayout.Height(700));

        #region Identify operator buttons
        var ope_1Style = new GUIStyle(GUI.skin.button);
        var ope_2Style = new GUIStyle(GUI.skin.button);
        var ope_3Style = new GUIStyle(GUI.skin.button);
        var ope_4Style = new GUIStyle(GUI.skin.button);
        var ope_5Style = new GUIStyle(GUI.skin.button);
        var ope_6Style = new GUIStyle(GUI.skin.button);
        var ope_7Style = new GUIStyle(GUI.skin.button);
        var ope_8Style = new GUIStyle(GUI.skin.button);
        var ope_9Style = new GUIStyle(GUI.skin.button);
        ope_1Style.fixedWidth = gridButtonSize;
        ope_1Style.fixedHeight = gridButtonSize;
        ope_2Style.fixedWidth = gridButtonSize;
        ope_2Style.fixedHeight = gridButtonSize;
        ope_3Style.fixedWidth = gridButtonSize;
        ope_3Style.fixedHeight = gridButtonSize;
        ope_4Style.fixedWidth = gridButtonSize;
        ope_4Style.fixedHeight = gridButtonSize;
        ope_5Style.fixedWidth = gridButtonSize;
        ope_5Style.fixedHeight = gridButtonSize;
        ope_6Style.fixedWidth = gridButtonSize;
        ope_6Style.fixedHeight = gridButtonSize;
        ope_7Style.fixedWidth = gridButtonSize;
        ope_7Style.fixedHeight = gridButtonSize;
        ope_8Style.fixedWidth = gridButtonSize;
        ope_8Style.fixedHeight = gridButtonSize;
        ope_9Style.fixedWidth = gridButtonSize;
        ope_9Style.fixedHeight = gridButtonSize;

        switch (selectedOpeNumber)
        {
            case 1:
                ope_1Style.normal.textColor = Color.magenta;
                ope_1Style.fontSize = 18;
                ope_2Style.normal.textColor = Color.black;
                ope_2Style.fontSize = 15;
                ope_3Style.normal.textColor = Color.black;
                ope_3Style.fontSize = 15;
                ope_4Style.normal.textColor = Color.black;
                ope_4Style.fontSize = 15;
                ope_5Style.normal.textColor = Color.black;
                ope_5Style.fontSize = 15;
                ope_6Style.normal.textColor = Color.black;
                ope_6Style.fontSize = 15;
                ope_7Style.normal.textColor = Color.black;
                ope_7Style.fontSize = 15;
                ope_8Style.normal.textColor = Color.black;
                ope_8Style.fontSize = 15;
                ope_9Style.normal.textColor = Color.black;
                ope_9Style.fontSize = 15;
                break;
            case 2:
                ope_1Style.normal.textColor = Color.black;
                ope_1Style.fontSize = 15;
                ope_2Style.normal.textColor = Color.magenta;
                ope_2Style.fontSize = 18;
                ope_3Style.normal.textColor = Color.black;
                ope_3Style.fontSize = 15;
                ope_4Style.normal.textColor = Color.black;
                ope_4Style.fontSize = 15;
                ope_5Style.normal.textColor = Color.black;
                ope_5Style.fontSize = 15;
                ope_6Style.normal.textColor = Color.black;
                ope_6Style.fontSize = 15;
                ope_7Style.normal.textColor = Color.black;
                ope_7Style.fontSize = 15;
                ope_8Style.normal.textColor = Color.black;
                ope_8Style.fontSize = 15;
                ope_9Style.normal.textColor = Color.black;
                ope_9Style.fontSize = 15;
                break;
            case 3:
                ope_1Style.normal.textColor = Color.black;
                ope_1Style.fontSize = 15;
                ope_2Style.normal.textColor = Color.black;
                ope_2Style.fontSize = 15;
                ope_3Style.normal.textColor = Color.magenta;
                ope_3Style.fontSize = 18;
                ope_4Style.normal.textColor = Color.black;
                ope_4Style.fontSize = 15;
                ope_5Style.normal.textColor = Color.black;
                ope_5Style.fontSize = 15;
                ope_6Style.normal.textColor = Color.black;
                ope_6Style.fontSize = 15;
                ope_7Style.normal.textColor = Color.black;
                ope_7Style.fontSize = 15;
                ope_8Style.normal.textColor = Color.black;
                ope_8Style.fontSize = 15;
                ope_9Style.normal.textColor = Color.black;
                ope_9Style.fontSize = 15;
                break;
            case 4:
                ope_1Style.normal.textColor = Color.black;
                ope_1Style.fontSize = 15;
                ope_2Style.normal.textColor = Color.black;
                ope_2Style.fontSize = 15;
                ope_3Style.normal.textColor = Color.black;
                ope_3Style.fontSize = 15;
                ope_4Style.normal.textColor = Color.magenta;
                ope_4Style.fontSize = 18;
                ope_5Style.normal.textColor = Color.black;
                ope_5Style.fontSize = 15;
                ope_6Style.normal.textColor = Color.black;
                ope_6Style.fontSize = 15;
                ope_7Style.normal.textColor = Color.black;
                ope_7Style.fontSize = 15;
                ope_8Style.normal.textColor = Color.black;
                ope_8Style.fontSize = 15;
                ope_9Style.normal.textColor = Color.black;
                ope_9Style.fontSize = 15;
                break;
            case 5:
                ope_1Style.normal.textColor = Color.black;
                ope_1Style.fontSize = 15;
                ope_2Style.normal.textColor = Color.black;
                ope_2Style.fontSize = 15;
                ope_3Style.normal.textColor = Color.black;
                ope_3Style.fontSize = 15;
                ope_4Style.normal.textColor = Color.black;
                ope_4Style.fontSize = 15;
                ope_5Style.normal.textColor = Color.magenta;
                ope_5Style.fontSize = 18;
                ope_6Style.normal.textColor = Color.black;
                ope_6Style.fontSize = 15;
                ope_7Style.normal.textColor = Color.black;
                ope_7Style.fontSize = 15;
                ope_8Style.normal.textColor = Color.black;
                ope_8Style.fontSize = 15;
                ope_9Style.normal.textColor = Color.black;
                ope_9Style.fontSize = 15;
                break;
            case 6:
                ope_1Style.normal.textColor = Color.black;
                ope_1Style.fontSize = 15;
                ope_2Style.normal.textColor = Color.black;
                ope_2Style.fontSize = 15;
                ope_3Style.normal.textColor = Color.black;
                ope_3Style.fontSize = 15;
                ope_4Style.normal.textColor = Color.black;
                ope_4Style.fontSize = 15;
                ope_5Style.normal.textColor = Color.black;
                ope_5Style.fontSize = 15;
                ope_6Style.normal.textColor = Color.magenta;
                ope_6Style.fontSize = 18;
                ope_7Style.normal.textColor = Color.black;
                ope_7Style.fontSize = 15;
                ope_8Style.normal.textColor = Color.black;
                ope_8Style.fontSize = 15;
                ope_9Style.normal.textColor = Color.black;
                ope_9Style.fontSize = 15;
                break;
            case 7:
                ope_1Style.normal.textColor = Color.black;
                ope_1Style.fontSize = 15;
                ope_2Style.normal.textColor = Color.black;
                ope_2Style.fontSize = 15;
                ope_3Style.normal.textColor = Color.black;
                ope_3Style.fontSize = 15;
                ope_4Style.normal.textColor = Color.black;
                ope_4Style.fontSize = 15;
                ope_5Style.normal.textColor = Color.black;
                ope_5Style.fontSize = 15;
                ope_6Style.normal.textColor = Color.black;
                ope_6Style.fontSize = 15;
                ope_7Style.normal.textColor = Color.magenta;
                ope_7Style.fontSize = 18;
                ope_8Style.normal.textColor = Color.black;
                ope_8Style.fontSize = 15;
                ope_9Style.normal.textColor = Color.black;
                ope_9Style.fontSize = 15;
                break;
            case 8:
                ope_1Style.normal.textColor = Color.black;
                ope_1Style.fontSize = 15;
                ope_2Style.normal.textColor = Color.black;
                ope_2Style.fontSize = 15;
                ope_3Style.normal.textColor = Color.black;
                ope_3Style.fontSize = 15;
                ope_4Style.normal.textColor = Color.black;
                ope_4Style.fontSize = 15;
                ope_5Style.normal.textColor = Color.black;
                ope_5Style.fontSize = 15;
                ope_6Style.normal.textColor = Color.black;
                ope_6Style.fontSize = 15;
                ope_7Style.normal.textColor = Color.black;
                ope_7Style.fontSize = 15;
                ope_8Style.normal.textColor = Color.magenta;
                ope_8Style.fontSize = 18;
                ope_9Style.normal.textColor = Color.black;
                ope_9Style.fontSize = 15;
                break;
            case 9:
                ope_1Style.normal.textColor = Color.black;
                ope_1Style.fontSize = 15;
                ope_2Style.normal.textColor = Color.black;
                ope_2Style.fontSize = 15;
                ope_3Style.normal.textColor = Color.black;
                ope_3Style.fontSize = 15;
                ope_4Style.normal.textColor = Color.black;
                ope_4Style.fontSize = 15;
                ope_5Style.normal.textColor = Color.black;
                ope_5Style.fontSize = 15;
                ope_6Style.normal.textColor = Color.black;
                ope_6Style.fontSize = 15;
                ope_7Style.normal.textColor = Color.black;
                ope_7Style.fontSize = 15;
                ope_8Style.normal.textColor = Color.black;
                ope_8Style.fontSize = 15;
                ope_9Style.normal.textColor = Color.magenta;
                ope_9Style.fontSize = 18;
                break;
            default:
                break;
        }
        #endregion

        GUILayout.Space(10);

        #region Total level
        GUILayout.Label("TOTAL LEVEL: " + LevelManager.GetTotalLevel(), EditorStyles.boldLabel);
        #endregion

        GUILayout.Space(10);

        #region Pre, Next And New Level
        GUILayout.Label("                        Current Level");
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.BeginHorizontal("Box", GUILayout.Width(boxGroupSize));

        float fieldWidth_1 = boxGroupSize / 4f;
        //Load previous level
        EditorGUI.BeginDisabledGroup(LevelManager.levelData.levelNumber <= 1);
        if (GUILayout.Button("<=", GUILayout.Height(height), GUILayout.Width(fieldWidth_1)))
        {
            LevelManager.levelData.levelNumber--;
            LevelManager.LoadLevel(LevelManager.levelData.levelNumber);
            listHint.Clear();
            for (int i = 0; i < LevelManager.levelData.listHintStep.Count; i++)
            {
                if (!listHint.Contains(LevelManager.levelData.listHintStep[i]))
                    listHint.Add(LevelManager.levelData.listHintStep[i]);
            }
        }
        EditorGUI.EndDisabledGroup();

        //Current level 
        LevelManager.levelData.levelNumber = EditorGUILayout.IntField(LevelManager.levelData.levelNumber, GUILayout.Height(height), GUILayout.Width(fieldWidth_1));

        //Load next level
        EditorGUI.BeginDisabledGroup(LevelManager.levelData.levelNumber >= LevelManager.GetTotalLevel());
        if (GUILayout.Button("=>", GUILayout.Height(height), GUILayout.Width(fieldWidth_1)))
        {
            LevelManager.levelData.levelNumber++;
            LevelManager.LoadLevel(LevelManager.levelData.levelNumber);
            listHint.Clear();
            for (int i = 0; i < LevelManager.levelData.listHintStep.Count; i++)
            {
                if (!listHint.Contains(LevelManager.levelData.listHintStep[i]))
                    listHint.Add(LevelManager.levelData.listHintStep[i]);
            }
        }
        EditorGUI.EndDisabledGroup();

        //Create new level
        bool disable;
        if (LevelManager.GetTotalLevel() == 0)
            disable = false;
        else
            disable = !LevelManager.IsLevelHasData(LevelManager.GetTotalLevel());
        EditorGUI.BeginDisabledGroup(disable);
        if (GUILayout.Button("New Level", GUILayout.Height(height), GUILayout.Width(fieldWidth_1)))
        {
            LevelManager.NewLevel();
            listHint.Clear();
        }
        EditorGUI.EndDisabledGroup();
        EditorGUILayout.EndHorizontal();

        if (LevelManager.GetTotalLevel() == 0)
            EditorGUILayout.HelpBox("No level found, please create new level!!!", MessageType.Warning, true);
        else if (disable)
            EditorGUILayout.HelpBox("Please save level " + LevelManager.GetTotalLevel().ToString() + " before create a new one!!!", MessageType.Warning, true);
        else
        {
            if (LevelManager.levelData.levelNumber <= 0 || LevelManager.levelData.levelNumber > LevelManager.GetTotalLevel())
                EditorGUILayout.HelpBox("Invalid level number!!!", MessageType.Error, true);
        }

        if (EditorGUI.EndChangeCheck())
        {
            if (LevelManager.levelData.levelNumber > 0 && LevelManager.levelData.levelNumber <= LevelManager.GetTotalLevel())
            {
                LevelManager.LoadLevel(LevelManager.levelData.levelNumber);
                listHint.Clear();
                for (int i = 0; i < LevelManager.levelData.listHintStep.Count; i++)
                {
                    if (!listHint.Contains(LevelManager.levelData.listHintStep[i]))
                        listHint.Add(LevelManager.levelData.listHintStep[i]);
                }
            }
        }

        #endregion

        GUILayout.Space(10);

        #region First Number, Goal And Moves
        EditorGUILayout.BeginHorizontal("Box", GUILayout.Width(boxGroupSize));

        //First number
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.LabelField("First Number:", GUILayout.Width(fieldWidth_1));
        LevelManager.levelData.firstNumber = EditorGUILayout.IntField(LevelManager.levelData.firstNumber, GUILayout.Height(height), GUILayout.Width(fieldWidth_1));

        //Moves
        EditorGUILayout.LabelField("Moves:" + LevelManager.levelData.listHintStep.Count.ToString(), GUILayout.Height(height), GUILayout.Width(fieldWidth_1));

        //Goal
        //Calculator goal
        bool isError = false;
        LevelManager.levelData.goal = Calculator.CalculateGoal(out isError);
        if (!isError)
        {
            EditorGUILayout.LabelField("Goal:" + LevelManager.levelData.goal.ToString(), GUILayout.Height(height), GUILayout.Width(fieldWidth_1));
        }
        else
        {
            EditorGUILayout.LabelField("ERROR", GUILayout.Height(height), GUILayout.Width(fieldWidth_1));
        }

        EditorGUILayout.EndHorizontal();
        #endregion

        GUILayout.Space(10);

        #region Is Have Better Solution ?

        bool isHaveBetterSolution = Calculator.IsHaveBetterSolution(
            LevelManager.levelData.firstNumber,
            listHint.ToArray(),
            LevelManager.levelData.listHintStep.Count,
            LevelManager.levelData.goal);

        EditorGUILayout.BeginHorizontal("Box", GUILayout.Width(boxGroupSize));

        string countAllCases = "(" + Calculator.CountAllCases + ") ";

        if (isHaveBetterSolution)
        {
            GUILayout.Label(countAllCases + Calculator.BetterSolution, EditorStyles.boldLabel);
        }
        else
        {
            GUILayout.Label(countAllCases + "Good solution", EditorStyles.boldLabel);
        }

        EditorGUILayout.EndHorizontal();
        #endregion

        GUILayout.Space(10);

        #region Operators Grid

        //Draw the operator_1, operator_2, operator_3
        GUILayout.BeginHorizontal();
        GUILayout.Space(40);

        //Draw operator_1
        if (GUILayout.Button(LevelManager.levelData.operator_1, ope_1Style))
        {
            selectedOpeNumber = 1;
            GUI.FocusControl(null);
        }

        //Draw operator_2
        if (GUILayout.Button(LevelManager.levelData.operator_2, ope_2Style))
        {
            selectedOpeNumber = 2;
            GUI.FocusControl(null);
        }

        //Draw operator_3
        if (GUILayout.Button(LevelManager.levelData.operator_3, ope_3Style))
        {
            selectedOpeNumber = 3;
            GUI.FocusControl(null);
        }
        GUILayout.EndHorizontal();




        //Draw the operator_4, operator_5 and operator_6
        GUILayout.BeginHorizontal();
        GUILayout.Space(40);

        //Operator_4
        if (GUILayout.Button(LevelManager.levelData.operator_4, ope_4Style))
        {
            selectedOpeNumber = 4;
            GUI.FocusControl(null);
        }

        //Operator_5
        if (GUILayout.Button(LevelManager.levelData.operator_5, ope_5Style))
        {
            selectedOpeNumber = 5;
            GUI.FocusControl(null);
        }

        //Operator_6
        if (GUILayout.Button(LevelManager.levelData.operator_6, ope_6Style))
        {
            selectedOpeNumber = 6;
            GUI.FocusControl(null);
        }
        GUILayout.EndHorizontal();




        //Draw the operator_7 and operator_8 and operator_9
        GUILayout.BeginHorizontal();
        GUILayout.Space(40);
        //Operator_7
        if (GUILayout.Button(LevelManager.levelData.operator_7, ope_7Style))
        {
            selectedOpeNumber = 7;
            GUI.FocusControl(null);
        }

        //Operator_8
        if (GUILayout.Button(LevelManager.levelData.operator_8, ope_8Style))
        {
            selectedOpeNumber = 8;
            GUI.FocusControl(null);
        }

        //Draw operator_9
        if (GUILayout.Button(LevelManager.levelData.operator_9, ope_9Style))
        {
            selectedOpeNumber = 9;
            GUI.FocusControl(null);
        }
        EditorGUILayout.EndHorizontal();

        #endregion

        GUILayout.Space(10);

        #region Operator Types Selection


        EditorGUILayout.BeginHorizontal("Box", GUILayout.Width(boxGroupSize));
        EditorGUI.BeginChangeCheck();

        //If the selected operator equals to string.Empty then the index is 0, otherwise
        //The index is the index of selected operator in the list

        string pickedOperator = PickOperator(selectedOpeNumber);
        int indexOfList = listOperator.IndexOf(RegexHandler.RemovedNums(pickedOperator));

        float width;
        if (OperatorTypes.ListOneNumOperator.Contains(listOperator[indexOfList]) || OperatorTypes.ListTwoNumsOperator.Contains(listOperator[indexOfList]))
            width = (boxGroupSize + 10f) / 2f;
        else
            width = boxGroupSize + 20f;

        //Draw the dropdown operator selection
        indexOfList = EditorGUILayout.Popup(indexOfList, listOperator.ToArray(), GUILayout.Width(width));

        int num_1 = 1;
        int num_2 = 2;

        List<string> listNums = RegexHandler.RemovedChars(pickedOperator);

        //Draw the numbers for operator that has numbers
        if (OperatorTypes.ListOneNumOperator.Contains(listOperator[indexOfList])) //This is the operator that has one number
        {
            if (listNums.Count > 0)
                num_1 = int.Parse(listNums[0]);
            num_1 = EditorGUILayout.IntField(num_1, GUILayout.Width((boxGroupSize + 10) / 2f));

        }
        else if (OperatorTypes.ListTwoNumsOperator.Contains(listOperator[indexOfList]))//This is the operator that has two numbers
        {
            if (listNums.Count > 1)
            {
                num_1 = int.Parse(listNums[0]);
                num_2 = int.Parse(listNums[1]);
            }
            num_1 = EditorGUILayout.IntField(num_1, GUILayout.Width((boxGroupSize + 10) / 4f));
            num_2 = EditorGUILayout.IntField(num_2, GUILayout.Width((boxGroupSize + 10) / 4f));
        }

        if (EditorGUI.EndChangeCheck())
        {
            if (OperatorTypes.ListOneNumOperator.Contains(listOperator[indexOfList]))
            {
                string ope = string.Empty;
                if (listOperator[indexOfList].Equals(OperatorTypes.RemoveNum))
                {
                    ope = "<" + num_1.ToString() + ">";
                }
                else
                {
                    ope = listOperator[indexOfList] + num_1.ToString();
                }
                SetOperator(ope, selectedOpeNumber);
            }
            else if (OperatorTypes.ListTwoNumsOperator.Contains(listOperator[indexOfList]))
            {
                string ope = num_1.ToString() + listOperator[indexOfList] + num_2.ToString();
                SetOperator(ope, selectedOpeNumber);
            }
            else
            {
                SetOperator(listOperator[indexOfList], selectedOpeNumber);
            }

            SetHints();

            GUI.FocusControl(null);
        }


        EditorGUILayout.EndHorizontal();

        #endregion

        GUILayout.Space(10);

        #region Hints

        //Draw hint steps on editor
        for (int i = 0; i < LevelManager.levelData.listHintStep.Count; i++)
        {
            GUILayout.BeginVertical("Box", GUILayout.Width(boxGroupSize));
            GUILayout.BeginHorizontal();

            //Draw step count
            GUILayout.Label("Step : " + (i + 1).ToString(), GUILayout.Width(boxGroupSize / 3f), GUILayout.Height(height));

            EditorGUI.BeginChangeCheck();
            int index = listHint.IndexOf(LevelManager.levelData.listHintStep[i]);
            index = EditorGUILayout.Popup(index, listHint.ToArray(), GUILayout.Width(boxGroupSize / 3f), GUILayout.Height(height));

            if (EditorGUI.EndChangeCheck())
            {
                LevelManager.levelData.listHintStep[i] = listHint[index];
                GUI.FocusControl(null);
            }

            //Daw remove button
            if (GUILayout.Button("Remove", GUILayout.Width(boxGroupSize / 3)))
            {
                LevelManager.levelData.listHintStep.Remove(LevelManager.levelData.listHintStep[i]);
                break;
            }
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
        }

        GUILayout.Space(10);

        //Button add hint step
        if (GUILayout.Button("Add Step", GUILayout.Width(boxGroupSize + 30), GUILayout.Height(height * 2)))
        {
            if (listHint.Count > 0)
                LevelManager.levelData.listHintStep.Add(listHint[0]);
        }

        #endregion

        GUILayout.Space(10);

        #region Save and overwrite levels

        if ((LevelManager.levelData.levelNumber > 0 && LevelManager.levelData.levelNumber <= LevelManager.GetTotalLevel()))
        {
            if (LevelManager.levelData.listHintStep.Count > 0)
            {
                string btnName;
                if (LevelManager.IsLevelHasData(LevelManager.levelData.levelNumber))
                    btnName = "OVERWRITE LEVEL";
                else
                    btnName = "SAVE LEVEL";
                if (GUILayout.Button(btnName, GUILayout.Width(boxGroupSize + 30), GUILayout.Height(height * 2)))
                {
                    LevelManager.SaveLevel();
                }
            }
        }
        #endregion

        GUILayout.EndScrollView();
    }



    string PickOperator(int selectedOpeGrid)
    {
        switch (selectedOpeGrid)
        {
            case 1:
                return LevelManager.levelData.operator_1;
            case 2:
                return LevelManager.levelData.operator_2;
            case 3:
                return LevelManager.levelData.operator_3;
            case 4:
                return LevelManager.levelData.operator_4;
            case 5:
                return LevelManager.levelData.operator_5;
            case 6:
                return LevelManager.levelData.operator_6;
            case 7:
                return LevelManager.levelData.operator_7;
            case 8:
                return LevelManager.levelData.operator_8;
            default:
                return LevelManager.levelData.operator_9;
        }
    }


    void SetOperator(string ope, int opeNumber)
    {
        switch (opeNumber)
        {
            case 1:
                LevelManager.levelData.operator_1 = ope;
                return;
            case 2:
                LevelManager.levelData.operator_2 = ope;
                return;
            case 3:
                LevelManager.levelData.operator_3 = ope;
                return;
            case 4:
                LevelManager.levelData.operator_4 = ope;
                return;
            case 5:
                LevelManager.levelData.operator_5 = ope;
                return;
            case 6:
                LevelManager.levelData.operator_6 = ope;
                return;
            case 7:
                LevelManager.levelData.operator_7 = ope;
                return;
            case 8:
                LevelManager.levelData.operator_8 = ope;
                return;
            default:
                LevelManager.levelData.operator_9 = ope;
                return;
        }
    }

    void SetHints()
    {
        listHint.Clear();
        if (!RegexHandler.RemovedNums(LevelManager.levelData.operator_1).Equals(OperatorTypes.None))
            listHint.Add(LevelManager.levelData.operator_1);
        if (!RegexHandler.RemovedNums(LevelManager.levelData.operator_2).Equals(OperatorTypes.None))
            listHint.Add(LevelManager.levelData.operator_2);
        if (!RegexHandler.RemovedNums(LevelManager.levelData.operator_3).Equals(OperatorTypes.None))
            listHint.Add(LevelManager.levelData.operator_3);
        if (!RegexHandler.RemovedNums(LevelManager.levelData.operator_4).Equals(OperatorTypes.None))
            listHint.Add(LevelManager.levelData.operator_4);
        if (!RegexHandler.RemovedNums(LevelManager.levelData.operator_5).Equals(OperatorTypes.None))
            listHint.Add(LevelManager.levelData.operator_5);
        if (!RegexHandler.RemovedNums(LevelManager.levelData.operator_6).Equals(OperatorTypes.None))
            listHint.Add(LevelManager.levelData.operator_6);
        if (!RegexHandler.RemovedNums(LevelManager.levelData.operator_7).Equals(OperatorTypes.None))
            listHint.Add(LevelManager.levelData.operator_7);
        if (!RegexHandler.RemovedNums(LevelManager.levelData.operator_8).Equals(OperatorTypes.None))
            listHint.Add(LevelManager.levelData.operator_8);
        if (!RegexHandler.RemovedNums(LevelManager.levelData.operator_9).Equals(OperatorTypes.None))
            listHint.Add(LevelManager.levelData.operator_9);
        for (int i = 0; i < LevelManager.levelData.listHintStep.Count; i++)
        {
            if (!listHint.Contains(LevelManager.levelData.listHintStep[i]))
            {
                LevelManager.levelData.listHintStep.Remove(LevelManager.levelData.listHintStep[i]);
            }
        }
    }

}
