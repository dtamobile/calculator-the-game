﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class LevelManager : MonoBehaviour {

    public static LevelData levelData = new LevelData();
    

    public static string LevelDataPath(int levelNumber)
    {
        return "Assets/BrainCalculator/Resources/Levels/" + levelNumber.ToString() + ".json";
    }


    /// <summary>
    /// Reset the level data
    /// </summary>
    public static void ResetLevelData()
    {
        if (GetTotalLevel() == 0)
            levelData = new LevelData();
        else
        {
            if (!IsLevelHasData(GetTotalLevel()))
            {
                levelData = new LevelData();
                levelData.levelNumber = GetTotalLevel();
            }
            else
            {
                LoadLevel(GetTotalLevel());
            }
        }
    }


    /// <summary>
    /// Get the total level number
    /// </summary>
    /// <returns></returns>
    public static int GetTotalLevel()
    {
        return Resources.LoadAll("Levels").Length;
    }


    /// <summary>
    /// Create new level
    /// </summary>
    public static void NewLevel()
    {
        FileStream fs = new FileStream(LevelDataPath(GetTotalLevel() + 1), FileMode.Create);
        fs.Close();
        levelData = new LevelData();
        levelData.levelNumber = GetTotalLevel() + 1;
#if UNITY_EDITOR
        AssetDatabase.Refresh();
#endif
    }


    /// <summary>
    /// Save the level to json file
    /// </summary>
    public static void SaveLevel()
    {
        string json = JsonUtility.ToJson(levelData);
        File.WriteAllText(LevelDataPath(levelData.levelNumber), json);
#if UNITY_EDITOR
        AssetDatabase.Refresh();
        EditorUtility.DisplayDialog("Level Saved!!", "Level " + levelData.levelNumber.ToString() + " was saved!!!", "OK");
#endif
    }


    /// <summary>
    /// Load the existing level 
    /// </summary>
    /// <param name="levelNumber"></param>
    public static void LoadLevel(int levelNumber)
    {
        if (!IsLevelHasData(levelNumber))
        {
            levelData = new LevelData();
            levelData.levelNumber = levelNumber;
        }
        else
        {
            string json = File.ReadAllText(LevelDataPath(levelNumber));
            levelData = JsonUtility.FromJson<LevelData>(json);
        }
    }



    /// <summary>
    /// Check the level's json file has data or not
    /// </summary>
    /// <param name="levelNumber"></param>
    /// <returns></returns>
    public static bool IsLevelHasData(int levelNumber)
    {
        string data = File.ReadAllText(LevelDataPath(levelNumber));
        return !string.IsNullOrEmpty(data);
    }
}
