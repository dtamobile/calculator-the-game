﻿using System.Collections.Generic;

public class OperatorTypes
{


    public static List<string> ListOneNumOperator
    {
        get
        {
            List<string> listOneNumOperator = new List<string>();
            listOneNumOperator.Add(Plus);
            listOneNumOperator.Add(Subtract);
            listOneNumOperator.Add(Multiple);
            listOneNumOperator.Add(Devide);
            listOneNumOperator.Add(Surplus);
            listOneNumOperator.Add(IncreaseNum);
            listOneNumOperator.Add(DecreaseNum);
            listOneNumOperator.Add(InsertToTheFirst);
            listOneNumOperator.Add(InsertToTheLast);
            listOneNumOperator.Add(Duplicate);
            listOneNumOperator.Add(RemoveNum);
            listOneNumOperator.Add(TenSubstractDigit);
            listOneNumOperator.Add(StoreResult);
            listOneNumOperator.Add(ChangeValueButtonPlus2);
            return listOneNumOperator;
        }
    }

    public static List<string> ListTwoNumsOperator
    {
        get
        {
            List<string> l = new List<string>();
            l.Add(ConvertNums);
            l.Add(Portals);
            return l;
        }
    }

    


    /// <summary>
    /// The none operator
    /// </summary>
    public const string None = "None";



    /// <summary>
    /// Plus the current number with the given number. Example:
    /// Curent number: 1
    /// Operator: +1
    /// Result: 2
    /// </summary>
    public const string Plus = "+";

    /// <summary>
    /// Substract the current number to the given number. Example:
    /// Current number: 1
    /// Operator: -1
    /// Result: 0
    /// </summary>
    public const string Subtract = "-";

    /// <summary>
    /// Multiple the current number with the given number. Example:
    /// Current number: 1
    /// Operator: x2
    /// Result: 2
    /// </summary>
    public const string Multiple = "x";

    /// <summary>
    /// Devide the current number for the given number. Example: 
    /// Current number: 5
    /// Operator: ÷2
    /// Result: 2
    /// ATTENTION: the result must be int.
    /// </summary>
    public const string Devide = "÷";



    /// <summary>
    /// Devide the current number for the given number and return the surplus number. Example: 
    /// Current number: 5
    /// Operator: %2
    /// Result: 1
    /// </summary>
    public const string Surplus = "%";


    /// <summary>
    /// Decrease all number in the string with the given value. Example:
    /// Current number: 1234
    /// Operator: ↓1
    /// Result: 123
    /// </summary>
    public const string DecreaseNum = "↓";


    /// <summary>
    /// Increase all number in the string with the given value. Example:
    /// Current number: 1234
    /// Operator: ↑1
    /// Result: 2345
    /// </summary>
    public const string IncreaseNum = "↑";

    /// <summary>
    /// Remove the last number of the current string. Example:
    /// Current number: 12345
    /// Operator: <<
    /// Result: 1234
    /// </summary>
    public const string RemoveLastNum = "<<";

    /// <summary>
    /// Remove the first number of the current string. Example:
    /// Current number: 12345
    /// Operator: >>
    /// Result: 2345
    /// </summary>
    public const string RemoveFirstNum = ">>";

    /// <summary>
    /// Insert a number to the last of the string. Example: 
    /// Current number: 1234
    /// Operator: <+5
    /// Result: 12345
    /// </summary>
    public const string InsertToTheLast = " ";

    /// <summary>
    /// Insert a number to the first of the string. Example: 
    /// Current number: 1234
    /// Operator: +>5
    /// Result: 51234
    /// </summary>
    public const string InsertToTheFirst = "+>";

    /// <summary>
    /// Convert the number from x to y. Example:
    /// Current number: 123
    /// Operator: 1=>2
    /// Result: 223
    /// </summary>
    public const string ConvertNums = "=>";

    /// <summary>
    /// Duplicate the current number with a given number. Example:
    /// Current number: 2
    /// Operator: ^3
    /// Result: 8
    /// </summary>
    public const string Duplicate = "^";

    /// <summary>
    /// Change the current number from positive/negative to negative/positive. Example: 
    /// Current number: -1
    /// Operator: +<=>-
    /// Result: 1
    /// </summary>
    public const string Opposite = @"+/-";

    /// <summary>
    /// Reverse the current number. Example: 
    /// Current number: 123
    /// Opertor: Reverse
    /// Result: 321
    /// </summary>
    public const string Reverse = "Reverse";

    /// <summary>
    /// Sum all the single number of the string. Example:
    /// Current number: 123
    /// Operator: Sum
    /// Result: 6
    /// </summary>
    public const string Sum = "Sum";

    /// <summary>
    /// Shift the current number to the left. Example:
    /// Current number: 1234
    /// Operator: <Shift
    /// Result: 2341
    /// </summary>
    public const string ShiftLeft = "<Shift";

    /// <summary>
    /// Shift the current number to the right. Example:
    /// Current number: 1234
    /// Operator: Shift>
    /// Result: 4123
    /// </summary>
    public const string ShiftRight = "Shift>";

    /// <summary>
    /// Mirror the current number. Example:
    /// Current number: 12
    /// Operator: Mirror
    /// Result: 1221
    /// </summary>
    public const string Mirror = "Mirror";


    /// <summary>
    /// Remove a given number if existed in the string. Example:
    /// Current number: 12345
    /// Operator: <23>
    /// Result: 145
    /// </summary>
    public const string RemoveNum = "<>";


    public const string Portals = "Portals";

    public const string TenSubstractDigit = "TenSubstract";

    public const string StoreResult = "StoreResult";

    public const string ChangeValueButtonPlus2 = "Plus2ToButton";

}
