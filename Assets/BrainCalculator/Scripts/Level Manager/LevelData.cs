﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelData  {

    public int levelNumber = 0;
    public int firstNumber = 0;
    public int goal = 0;
    public string operator_1 = "None";
    public string operator_2 = "None";
    public string operator_3 = "";
    public string operator_4 = "None";
    public string operator_5 = "None";
    public string operator_6 = "None";
    public string operator_7 = "None";
    public string operator_8 = "None";
    public string operator_9 = "None";
    public List<string> listHintStep = new List<string>();
}
