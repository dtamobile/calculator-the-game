﻿using UnityEngine;
using System;
using System.Collections;

namespace OnefallGames
{
    public class HintManager : MonoBehaviour
    {
        public static HintManager Instance;

        public int Hints
        { 
            get { return currentHints; }
            private set { currentHints = value; }
        }

        public static event Action<int> CoinsUpdated = delegate {};

        [SerializeField]
        int initialHints = 0;

        // Show the current coins value in editor for easy testing
        [SerializeField]
        int currentHints;

        // key name to store high score in PlayerPrefs
        const string PPK_COINS = "ONEFALL_COINS";


        void Awake()
        {
            if (Instance)
            {
                DestroyImmediate(gameObject);
            }
            else
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        void Start()
        {
            Reset();
        }

        public void Reset()
        {
            // Initialize coins
            Hints = PlayerPrefs.GetInt(PPK_COINS, initialHints);
        }

        public void AddHints(int amount)
        {
            Hints += amount;


            // Store new coin value
            PlayerPrefs.SetInt(PPK_COINS, Hints);

            // Fire event
            CoinsUpdated(Hints);
        }

        public void RemoveHints(int amount)
        {
            Hints -= amount;

            // Store new coin value
            PlayerPrefs.SetInt(PPK_COINS, Hints);

            // Fire event
            CoinsUpdated(Hints);
        }
    }
}
