﻿using System.Text;
using UnityEngine;
using OnefallGames;
using UnityEngine.SceneManagement;
using Facebook.Unity;
public enum GameState
{
    Prepare,
    Tutorial,
    Playing,
    PassLevel,
    DoneAllLevel,
    OutOfMoves,
}

public enum AdType
{
    VideoAd,
    RewardedVideoAd,
}

public class GameManager : MonoBehaviour
{

    public static GameManager Instance { private set; get; }
    public static event System.Action<GameState> GameStateChanged = delegate { };
    public static int levelLoaded = 1;
    private const string LevelSolvedKey = "LEVEL_SOLVED";
    public GameState GameState
    {
        get
        {
            return gameState;
        }
        private set
        {
            if (value != gameState)
            {
                gameState = value;
                GameStateChanged(gameState);
            }
        }
    }

    [Header("Check to unlock all levels (for testing purpose). Remember uncheck when build.")]
    [SerializeField]
    private bool unlockAllLevels;

    [Header("Gameplay Config")]
    public string outOfMovesText;
    public string[] DoneAllLevelText;
    public int chooseLevel;
    int PreviousLevel = 0;
    int hintCountSave = 0;
    public LevelData LevelData { private set; get; }
    public int CurrentNumber { private set; get; }
    public int Moves { private set; get; }
    public int HintStepCount { private set; get; }
    public bool IsError = false;

    int passLevelCount = 0;
    public bool m_IsOutOfMove = false;

    private GameState gameState = GameState.PassLevel;
    private bool isFinishedCalculate = false;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            DestroyImmediate(Instance.gameObject);
            Instance = this;
        }
    }

    void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }

    // Use this for initialization
    void Start()
    {
        //ServicesManager.instance.InitServices(true, true, false);
        //ServicesManager.Ads().LoadAndShowBannerBot();
        //ServicesManager.Ads().LoadFullAds();
        Application.targetFrameRate = 60;
        levelLoaded = MaxLevelSolved() + 1;
        PrepareGame();
        StartGame();
    }

    public void PrepareGame()
    {
        //Load the level data before fire event      
        LoadLevel();


        //Fire event
        GameState = GameState.Prepare;
        gameState = GameState.Prepare;

        //Add other actions here

    }


    /// <summary>
    /// Actual start the game
    /// </summary>
    public void StartGame()
    {
        if (TutorialManager.Instance.InitTut(levelLoaded))
        {
            GameState = GameState.Tutorial;
            gameState = GameState.Tutorial;
        }
        else
        {
            GameState = GameState.Playing;
            gameState = GameState.Playing;
        }
    }

    public void ShowPassAllLevel()
    {
        GameState = GameState.DoneAllLevel;
        gameState = GameState.DoneAllLevel;
    }

    public void ClearGame()
    {
        GameState = GameState.Playing;
        gameState = GameState.Playing;
    }
    /// <summary>
    /// Call pass level event
    /// </summary>
    public void PassLevel()
    {
        //passLevelCount++;
        //if (passLevelCount % 2 == 0)
        //{
        //    ServicesManager.Ads().ShowFullAds(() =>
        //    {

        //        //Fire event
        //        GameState = GameState.PassLevel;
        //        gameState = GameState.PassLevel;
        //        UIManager.Instance.SetMoves(0);
        //        //Add another actions here
        //        SaveLevel();
        //        SoundManager.Instance.PlaySound(SoundManager.Instance.win);
        //    });
        //}
        //else
        //{
        //    //Fire event
        //    GameState = GameState.PassLevel;
        //    gameState = GameState.PassLevel;
        //    UIManager.Instance.SetMoves(0);
        //    //Add another actions here
        //    SaveLevel();
        //    SoundManager.Instance.PlaySound(SoundManager.Instance.win);
        //}
        GameState = GameState.PassLevel;
        gameState = GameState.PassLevel;
        UIManager.Instance.SetMoves(0);
        //Add another actions here
        SaveLevel();
        SoundManager.Instance.PlaySound(SoundManager.Instance.win);
    }

    /// <summary>
    /// Call out of moves event
    /// </summary>
    public void OutOfMoves()
    {
        //Fire event
        GameState = GameState.OutOfMoves;
        gameState = GameState.OutOfMoves;
        m_IsOutOfMove = true;
        //Add another function here
        SoundManager.Instance.PlaySound(SoundManager.Instance.outOfMoves);
    }

    public void OnTutorialDone()
    {
        GameState = GameState.Playing;
        gameState = GameState.Playing;
    }

    /// <summary>
    /// Save the level number after passed
    /// </summary>
    void SaveLevel()
    {
        string data = PlayerPrefs.GetString(LevelSolvedKey);
        if (string.IsNullOrEmpty(data))
        {
            PlayerPrefs.SetString(LevelSolvedKey, LevelData.levelNumber.ToString());
        }
        else
        {
            StringBuilder builder = new StringBuilder(PlayerPrefs.GetString(LevelSolvedKey));
            foreach (string o in builder.ToString().Split(','))
            {
                if (o.Equals(LevelData.levelNumber.ToString()))
                    return;
            }
            builder.Append("," + LevelData.levelNumber.ToString());
            PlayerPrefs.SetString(LevelSolvedKey, builder.ToString());
        }
    }

    //////////////////////////////////////Publish functions
    public void LoadLevel()
    {
        if (IsSameLevel())
        {
            HintStepCount = hintCountSave;
        }
        else
        {
            HintStepCount = hintCountSave = 0;
        }

        Calculator.storeNumber = -1;
        TextAsset ta = Resources.Load<TextAsset>("Levels/" + levelLoaded.ToString());
        LevelData = JsonUtility.FromJson<LevelData>(ta.ToString());
        CurrentNumber = LevelData.firstNumber;
        Moves = LevelData.listHintStep.Count;
        isFinishedCalculate = false;
        IsError = false;
        //ClearHintStep();
        //ShowAllHintSave();

    }

    public void SetPreviousLevel()
    {
        PreviousLevel = levelLoaded;
    }

    public bool IsSameLevel()
    {
        if (PreviousLevel < 1)
        {
            return false;
        }
        if (levelLoaded == PreviousLevel)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    /// <summary>
    /// Get the max level that has solved
    /// </summary>
    /// <returns></returns>
    public int MaxLevelSolved()
    {
        string data = PlayerPrefs.GetString(LevelSolvedKey);
        int max = 0;
        if (unlockAllLevels)
        {
            //max = TotalLevel() - 1;
            max = chooseLevel;
        }
        else if (!string.IsNullOrEmpty(data))
        {
            foreach (string o in data.Split(','))
            {
                if (int.Parse(o) > max)
                    max = int.Parse(o);
            }
        }
        return max;
    }


    /// <summary>
    /// Get the total level
    /// </summary>
    /// <returns></returns>
    public int TotalLevel()
    {
        return Resources.LoadAll("Levels").Length;
    }


    /// <summary>
    /// Calculate result with the given operator
    /// </summary>
    /// <param name="ope"></param>
    public void CalculateNumber(string ope)
    {
        float? floatResult = null;
        if (gameState == GameState.Playing)
        {
            if (!isFinishedCalculate)
            {
                if (CurrentNumber != LevelData.goal)
                    CurrentNumber = Calculator.CalculateSingleOperator(CurrentNumber, ope, out floatResult);

                if (floatResult != null)
                {
                    if (floatResult.Value - CurrentNumber != 0)
                    {
                        isFinishedCalculate = true;
                        Invoke("OutOfMoves", 0.1f);
                        IsError = true;
                        return;
                    }
                }

                if (CurrentNumber == LevelData.goal)
                {
                    isFinishedCalculate = true;
                    Invoke("PassLevel", 0.1f);
                    return;
                }

                if (Moves > 0)
                    Moves--;

                if (Moves == 0)
                {
                    isFinishedCalculate = true;
                    Invoke("OutOfMoves", 0.01f);
                }

                if (CurrentNumber.ToString().Length >= 6)
                {
                    isFinishedCalculate = true;
                    Invoke("OutOfMoves", 0.01f);
                    IsError = true;
                }
            }

        }
    }



    /// <summary>
    /// Show the hints step
    /// </summary>
    public void ShowHintSteps()
    {
        if (HintManager.Instance.Hints > 0)
        {
            if (HintStepCount < LevelData.listHintStep.Count)
            {
                foreach (OperatorButtonController o in FindObjectsOfType<OperatorButtonController>())
                {
                    o.ShowHintSteps();
                }
                HintStepCount++;
                HintManager.Instance.RemoveHints(1);
                hintCountSave = HintStepCount;
            }
        }
    }
}
