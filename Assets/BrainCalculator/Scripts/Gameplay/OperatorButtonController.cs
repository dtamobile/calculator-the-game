﻿using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class OperatorButtonController : MonoBehaviour
{

    // Use this for initialization
    [SerializeField]
    private int operatorCount = 1;

    public Text operatorText = null;
    [SerializeField]
    private Text hintsTxt = null;
    public Sprite HiddenImage;
    public Sprite WhiteImage;

    public void SetPlayingUI()
    {
        var BtnScript = gameObject.GetComponent<EventTrigger>();
        if (operatorCount == 1 && !GameManager.Instance.LevelData.operator_1.Equals(OperatorTypes.None))
        {
            operatorText.text = GameManager.Instance.LevelData.operator_1;
            BtnScript.enabled = true;
            gameObject.SetActive(true);
        }
        else if (operatorCount == 2 && !GameManager.Instance.LevelData.operator_2.Equals(OperatorTypes.None))
        {
            operatorText.text = GameManager.Instance.LevelData.operator_2;
            BtnScript.enabled = true;
            gameObject.SetActive(true);
        }
        else if (operatorCount == 3)
        {
            operatorText.text = "CLR";
            BtnScript.enabled = true;
            gameObject.SetActive(true);
        }
        else if (operatorCount == 4 && !GameManager.Instance.LevelData.operator_4.Equals(OperatorTypes.None))
        {
            operatorText.text = GameManager.Instance.LevelData.operator_4;
            BtnScript.enabled = true;
            gameObject.SetActive(true);
        }
        else if (operatorCount == 5 && !GameManager.Instance.LevelData.operator_5.Equals(OperatorTypes.None))
        {
            operatorText.text = GameManager.Instance.LevelData.operator_5;
            BtnScript.enabled = true;
            gameObject.SetActive(true);
        }
        else if (operatorCount == 6 && !GameManager.Instance.LevelData.operator_6.Equals(OperatorTypes.None))
        {
            operatorText.text = GameManager.Instance.LevelData.operator_6;
            BtnScript.enabled = true;
            gameObject.SetActive(true);
        }
        else if (operatorCount == 7 && !GameManager.Instance.LevelData.operator_7.Equals(OperatorTypes.None))
        {
            operatorText.text = GameManager.Instance.LevelData.operator_7;
            BtnScript.enabled = true;
            gameObject.SetActive(true);
        }
        else if (operatorCount == 8 && !GameManager.Instance.LevelData.operator_8.Equals(OperatorTypes.None))
        {
            operatorText.text = GameManager.Instance.LevelData.operator_8;
            BtnScript.enabled = true;
            gameObject.SetActive(true);
        }
        else if (operatorCount == 9 && !GameManager.Instance.LevelData.operator_9.Equals(OperatorTypes.None))
        {
            operatorText.text = GameManager.Instance.LevelData.operator_9;
            BtnScript.enabled = true;
            gameObject.SetActive(true);
        }
        else if (operatorCount != 1 && operatorCount != 3 && operatorCount != 4 && operatorCount != 7)
        {
            operatorText.text = string.Empty;
            BtnScript.enabled = false;
            gameObject.SetActive(false);
        }

        //if (operatorText.text.Length <= 3)
        //    operatorText.fontSize = 50;
        //else if (operatorText.text.Length <= 7)
        //    operatorText.fontSize = 32;
        //else
        //    operatorText.fontSize = 25;
        
        hintsTxt.text = string.Empty;
    }

    public void HandleOnclick()
    {
        if (!string.IsNullOrEmpty(operatorText.text))
        {
            GameManager.Instance.CalculateNumber(operatorText.text);
            UIManager.Instance.StartCalculateText();
        }
    }


    public void ShowHintSteps()
    {
        if (operatorText.text.Equals(GameManager.Instance.LevelData.listHintStep[GameManager.Instance.HintStepCount]))
        {
            if (string.IsNullOrEmpty(hintsTxt.text))
            {
                hintsTxt.text = (GameManager.Instance.HintStepCount + 1).ToString();
            }
            else
            {
                StringBuilder builder = new StringBuilder(hintsTxt.text);
                builder.Append("," + (GameManager.Instance.HintStepCount + 1).ToString());
                hintsTxt.text = builder.ToString();
            }
        }
    }

    public void SetColorBtn1()
    {
        ResetColorBtn1();
        var img1 = UIManager.Instance.arrayBtn[1].GetComponent<Image>();
        var shadow1 = UIManager.Instance.arrayBtn[1].GetComponent<Shadow>();
        //ResetColorBtn1();
        if (GameManager.Instance.LevelData.operator_2.Equals(OperatorTypes.RemoveFirstNum))
        {
            img1.color = UIManager.Instance.OpeColor[0];
            shadow1.effectColor = UIManager.Instance.OpeColor[1];
        }
        else if (GameManager.Instance.LevelData.operator_2.Equals(OperatorTypes.RemoveLastNum) || GameManager.Instance.LevelData.operator_2.Equals(OperatorTypes.Opposite))
        {
            img1.color = UIManager.Instance.OpeColor[0];
            shadow1.effectColor = UIManager.Instance.OpeColor[1];
        }
        else if(GameManager.Instance.LevelData.operator_2.Contains(OperatorTypes.InsertToTheLast))
        {
            img1.color = UIManager.Instance.OpeColor[4];
            shadow1.effectColor = UIManager.Instance.OpeColor[5];
        }
        else if(GameManager.Instance.LevelData.operator_2.Contains(OperatorTypes.Reverse))
        {
            img1.color = UIManager.Instance.OpeColor[0];
            shadow1.effectColor = UIManager.Instance.OpeColor[1];
        }
    }


    public int GetOpeCount()
    {
        return operatorCount;
    }


    public void SetColorBtn2()
    {
        ResetColorBtn2();
        var img1 = UIManager.Instance.arrayBtn[4].GetComponent<Image>();
        var shadow1 = UIManager.Instance.arrayBtn[4].GetComponent<Shadow>();
        //ResetColorBtn1();
        if (operatorCount == 5 && GameManager.Instance.LevelData.operator_5.Equals(OperatorTypes.RemoveFirstNum))
        {
            img1.color = UIManager.Instance.OpeColor[0];
            shadow1.effectColor = UIManager.Instance.OpeColor[1];
        }
        else if (operatorCount == 5 && GameManager.Instance.LevelData.operator_5.Equals(OperatorTypes.RemoveLastNum) || GameManager.Instance.LevelData.operator_5.Equals(OperatorTypes.Opposite))
        {
            img1.color = UIManager.Instance.OpeColor[0];
            shadow1.effectColor = UIManager.Instance.OpeColor[1];
        }
        else if (operatorCount == 5 && GameManager.Instance.LevelData.operator_5.Contains(OperatorTypes.InsertToTheLast))
        {
            Debug.Log("Ope Insert Last2");
            img1.color = UIManager.Instance.OpeColor[4];
            shadow1.effectColor = UIManager.Instance.OpeColor[5];
        }
        else if (GameManager.Instance.LevelData.operator_5.Contains(OperatorTypes.Reverse))
        {
            img1.color = UIManager.Instance.OpeColor[0];
            shadow1.effectColor = UIManager.Instance.OpeColor[1];
        }
    }


    public void SetColorBtn3()
    {
        ResetColorBtn3();
        var img1 = UIManager.Instance.arrayBtn[5].GetComponent<Image>();
        var shadow1 = UIManager.Instance.arrayBtn[5].GetComponent<Shadow>();
        //ResetColorBtn1();
        if (operatorCount == 6 && GameManager.Instance.LevelData.operator_6.Equals(OperatorTypes.RemoveFirstNum))
        {
            img1.color = UIManager.Instance.OpeColor[0];
            shadow1.effectColor = UIManager.Instance.OpeColor[1];
        }
        else if (operatorCount == 6 && GameManager.Instance.LevelData.operator_6.Equals(OperatorTypes.RemoveLastNum) || GameManager.Instance.LevelData.operator_6.Equals(OperatorTypes.Opposite))
        {
            img1.color = UIManager.Instance.OpeColor[0];
            shadow1.effectColor = UIManager.Instance.OpeColor[1];
        }
        else if (operatorCount == 6 && GameManager.Instance.LevelData.operator_6.Contains(OperatorTypes.InsertToTheLast))
        {
            Debug.Log("Ope Insert Last3");
            img1.color = UIManager.Instance.OpeColor[4];
            shadow1.effectColor = UIManager.Instance.OpeColor[5];
        }
        else if (GameManager.Instance.LevelData.operator_6.Contains(OperatorTypes.Reverse))
        {
            img1.color = UIManager.Instance.OpeColor[0];
            shadow1.effectColor = UIManager.Instance.OpeColor[1];
        }
    }


    public void SetColorBtn4()
    {
        ResetColorBtn4();
        var img1 = UIManager.Instance.arrayBtn[7].GetComponent<Image>();
        var shadow1 = UIManager.Instance.arrayBtn[7].GetComponent<Shadow>();
        //ResetColorBtn1();
        if (operatorCount == 8 && GameManager.Instance.LevelData.operator_8.Equals(OperatorTypes.RemoveFirstNum))
        {
            img1.color = UIManager.Instance.OpeColor[0];
            shadow1.effectColor = UIManager.Instance.OpeColor[1];
        }
        else if (operatorCount == 8 && GameManager.Instance.LevelData.operator_8.Equals(OperatorTypes.RemoveLastNum) || GameManager.Instance.LevelData.operator_8.Equals(OperatorTypes.Opposite))
        {
            img1.color = UIManager.Instance.OpeColor[0];
            shadow1.effectColor = UIManager.Instance.OpeColor[1];
        }
        else if (operatorCount == 8 && GameManager.Instance.LevelData.operator_8.Contains(OperatorTypes.InsertToTheLast))
        {
            Debug.Log("Ope Insert Last4");
            img1.color = UIManager.Instance.OpeColor[4];
            shadow1.effectColor = UIManager.Instance.OpeColor[5];
        }
        else if (GameManager.Instance.LevelData.operator_8.Contains(OperatorTypes.Reverse))
        {
            img1.color = UIManager.Instance.OpeColor[0];
            shadow1.effectColor = UIManager.Instance.OpeColor[1];
        }
    }


    public void SetColorBtn5()
    {
        ResetColorBtn5();
        var img1 = UIManager.Instance.arrayBtn[8].GetComponent<Image>();
        var shadow1 = UIManager.Instance.arrayBtn[8].GetComponent<Shadow>();
        //ResetColorBtn1();
        if (operatorCount == 9 && GameManager.Instance.LevelData.operator_9.Equals(OperatorTypes.RemoveFirstNum))
        {
            img1.color = UIManager.Instance.OpeColor[0];
            shadow1.effectColor = UIManager.Instance.OpeColor[1];
        }
        else if (operatorCount == 9 && GameManager.Instance.LevelData.operator_9.Equals(OperatorTypes.RemoveLastNum) || GameManager.Instance.LevelData.operator_9.Equals(OperatorTypes.Opposite))
        {
            img1.color = UIManager.Instance.OpeColor[0];
            shadow1.effectColor = UIManager.Instance.OpeColor[1];
        }
        else if (operatorCount == 9 && GameManager.Instance.LevelData.operator_9.Contains(OperatorTypes.InsertToTheLast))
        {
            Debug.Log("Ope Insert Last5");
            img1.color = UIManager.Instance.OpeColor[4];
            shadow1.effectColor = UIManager.Instance.OpeColor[5];
        }
        else if (GameManager.Instance.LevelData.operator_9.Contains(OperatorTypes.Reverse))
        {
            img1.color = UIManager.Instance.OpeColor[0];
            shadow1.effectColor = UIManager.Instance.OpeColor[1];
        }
    }

    public void ResetColorBtn1()
    {
        var img1 = UIManager.Instance.arrayBtn[1].GetComponent<Image>();
        var shadow1 = UIManager.Instance.arrayBtn[1].GetComponent<Shadow>();
        img1.color = UIManager.Instance.OpeColor[2];
        shadow1.effectColor = UIManager.Instance.OpeColor[3];
    }

    public void ResetColorBtn2()
    {
        var img1 = UIManager.Instance.arrayBtn[4].GetComponent<Image>();
        var shadow1 = UIManager.Instance.arrayBtn[4].GetComponent<Shadow>();
        img1.color = UIManager.Instance.OpeColor[2];
        shadow1.effectColor = UIManager.Instance.OpeColor[3];
    }

    public void ResetColorBtn3()
    {
        var img1 = UIManager.Instance.arrayBtn[5].GetComponent<Image>();
        var shadow1 = UIManager.Instance.arrayBtn[5].GetComponent<Shadow>();
        img1.color = UIManager.Instance.OpeColor[2];
        shadow1.effectColor = UIManager.Instance.OpeColor[3];
    }

    public void ResetColorBtn4()
    {
        var img1 = UIManager.Instance.arrayBtn[7].GetComponent<Image>();
        var shadow1 = UIManager.Instance.arrayBtn[7].GetComponent<Shadow>();
        img1.color = UIManager.Instance.OpeColor[2];
        shadow1.effectColor = UIManager.Instance.OpeColor[3];
    }

    public void ResetColorBtn5()
    {
        var img1 = UIManager.Instance.arrayBtn[8].GetComponent<Image>();
        var shadow1 = UIManager.Instance.arrayBtn[8].GetComponent<Shadow>();
        img1.color = UIManager.Instance.OpeColor[2];
        shadow1.effectColor = UIManager.Instance.OpeColor[3];
    }
}
