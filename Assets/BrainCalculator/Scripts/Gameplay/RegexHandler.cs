﻿using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;
using UnityEngine;
public class RegexHandler {

    

    /// <summary>
    /// Remove all number of the string.
    /// </summary>
    /// <param name="input"></param>
    /// <returns>Return the string that removed numbers</returns>
    public static string RemovedNums(string input)
    {
        StringBuilder builder = new StringBuilder();
        foreach(char i in input)
        {
            if (!char.IsNumber(i))
                builder.Append(i);
        }
        return builder.ToString();
    }



    /// <summary>
    /// Remove all char of the input string and return the list that contain all the number in the string.
    /// </summary>
    /// <param name="input"></param>
    /// <returns>The list of string that contain all number in the string input </returns>
	public static List<string> RemovedChars(string input)
    {
        List<string> listResult = new List<string>();
        string removedNums = RemovedNums(input);
        StringBuilder builder = new StringBuilder();
    
        if (OperatorTypes.ListTwoNumsOperator.Contains(removedNums)) //This is operator with two numbers
        {
            for(int i = 0; i < input.Length; i++)
            {
                if (char.IsNumber(input[i]))
                {
                    builder.Append(input[i]);
                    if (i == input.Length - 1)
                    {
                        listResult.Add(builder.ToString());
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(builder.ToString()))
                    {
                        listResult.Add(builder.ToString());
                    }
                    builder = new StringBuilder();
                }
            }
        }
        else //This is operator with one number
        {
            foreach (char i in input)
            {
                if (char.IsNumber(i))
                    builder.Append(i);
            }
            if (!string.IsNullOrEmpty(builder.ToString()))
                listResult.Add(builder.ToString());
        }
        return listResult;
    }
    
}
