﻿using OnefallGames;
using Prime31.ZestKit;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;
using Random = UnityEngine.Random;
public class UIManager : MonoBehaviour
{

    public static UIManager Instance { private set; get; }

    int gamePassCount = 0;
    public GameObject GamePlayUI;
    [SerializeField]
    private GameObject operatorButtons;
    [SerializeField]
    private GameObject settingButtons;
    [SerializeField]
    private GameObject gameplayButtons;
    [SerializeField]
    private GameObject soundOnBtn;
    [SerializeField]
    private GameObject soundOffBtn;
    [SerializeField]
    private GameObject showHintsBtn;
    [SerializeField]
    private GameObject getHintsBtn;
    [SerializeField]
    private GameObject clearBtn;
    [SerializeField]
    private GameObject nextBtn;
    [SerializeField]
    private GameObject gameNameTxt;
    [SerializeField]
    private GameObject getHintsPanel;
    [SerializeField]
    private Text levelTxt;
    [SerializeField]
    private Text goalTxt;
    [SerializeField]
    private Text movesTxt;
    [SerializeField]
    private Text currentNumberTxt;
    [SerializeField]
    private Text selectedLevelTxt;
    [SerializeField]
    private Text hintTxt;
    public GameObject TutorialCanvas;
    public GameObject PlayerCanvas;
    public GameObject FaceEmotion;
    public GameObject[] arrFace;
    public GameObject WinText;
    public GameObject PauseCanvas;
    public GameObject FakeOperatorButton;
    public GameObject IngameAtt;
    public OperatorButtonController[] arrayBtn;
    public GameObject[] arrPauseBtn;
    public GameObject BuyHintCanvas;
    bool isLoadNewLvl = true;
    public Text TutorialText;
    public GameObject[] arrFakeBtn;
    public GameObject DoneAllLevelCanvas;
    public Text DoneAllLevelText;
    public Color[] OpeColor;
    bool m_isLengthShort = true;
    Button[] opeScript;

    RectTransform BuyHintRect;
    RectTransform PlayerCanvasRect;
    Vector3 PlayerCanvasRectPos;
    Vector3 BuyHintRectPos;
    Vector3[] m_ArrPauseBtnRectPos;
    Shadow[] m_ArrPauseBtnShadow;
    RectTransform[] m_ArrPauseBtnRect;
    RectTransform[] m_ArrBtnRect;
    Vector3[] m_ArrBtnRectPos;
    Shadow[] m_ArrBtnShadow;
    int passLevelCount = 0;
    bool isWin = true;
    Coroutine winCoroutine;
    private Animator currentNumberAnim = null;
    private void OnEnable()
    {
        GameManager.GameStateChanged += GameManager_GameStateChanged;
    }


    private void OnDisable()
    {
        GameManager.GameStateChanged -= GameManager_GameStateChanged;
    }

    private void GameManager_GameStateChanged(GameState obj)
    {
        if (obj.Equals(GameState.Prepare))
        {
            ShowPrepareGameUI();
        }
        else if (obj.Equals(GameState.Tutorial))
        {
            ShowTutorialUI();
        }
        else if (obj.Equals(GameState.Playing))
        {
            ShowStartGameUI();
        }
        else if (obj.Equals(GameState.PassLevel))
        {
            ShowPassLevelUI();
        }
        else if (obj.Equals(GameState.DoneAllLevel))
        {
            ShowDoneAllLevelUI();
        }
        else
        {
            ShowOutOfMovesUI();
        }
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            DestroyImmediate(Instance.gameObject);
            Instance = this;
        }
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            //Handle FB.Init
            FB.Init(() =>
            {
                FB.ActivateApp();
            });
        }
        DataManager.instance.LoadData();
    }

    void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }

    private void Start()
    {
        currentNumberAnim = currentNumberTxt.GetComponent<Animator>();
        currentNumberAnim.enabled = false;
        getHintsPanel.SetActive(false);
        m_ArrBtnRect = new RectTransform[arrayBtn.Length];
        for (int i = 0; i < arrayBtn.Length; i++)
        {
            m_ArrBtnRect[i] = arrayBtn[i].gameObject.GetComponent<RectTransform>();
        }

        m_ArrBtnRectPos = new Vector3[arrayBtn.Length];
        for (int i = 0; i < arrayBtn.Length; i++)
        {
            m_ArrBtnRectPos[i] = m_ArrBtnRect[i].anchoredPosition;
        }

        m_ArrBtnShadow = new Shadow[arrayBtn.Length];
        for (int i = 0; i < arrayBtn.Length; i++)
        {
            m_ArrBtnShadow[i] = arrayBtn[i].gameObject.GetComponent<Shadow>();
        }
        m_ArrPauseBtnRect = new RectTransform[arrPauseBtn.Length];
        for (int i = 0; i < arrPauseBtn.Length; i++)
        {
            m_ArrPauseBtnRect[i] = arrPauseBtn[i].GetComponent<RectTransform>();
        }

        m_ArrPauseBtnRectPos = new Vector3[m_ArrPauseBtnRect.Length];
        for (int i = 0; i < arrPauseBtn.Length; i++)
        {
            m_ArrPauseBtnRectPos[i] = m_ArrPauseBtnRect[i].anchoredPosition;
        }

        m_ArrPauseBtnShadow = new Shadow[arrPauseBtn.Length];
        for (int i = 0; i < arrPauseBtn.Length; i++)
        {
            m_ArrPauseBtnShadow[i] = arrPauseBtn[i].GetComponent<Shadow>();
        }

        BuyHintRect = BuyHintCanvas.GetComponent<RectTransform>();
        BuyHintRectPos = BuyHintRect.anchoredPosition;
        PlayerCanvasRect = PlayerCanvas.GetComponent<RectTransform>();
        PlayerCanvasRectPos = PlayerCanvasRect.anchoredPosition;
        opeScript = new Button[3];
        opeScript[0] = arrayBtn[0].gameObject.GetComponent<Button>();
        opeScript[1] = arrayBtn[3].gameObject.GetComponent<Button>();
        opeScript[2] = arrayBtn[6].gameObject.GetComponent<Button>();
        ServicesManager.Instance.InitServices(true, true, false);
        ServicesManager.Ads().LoadBannerBot();
        ServicesManager.Ads().LoadFullAds();
        if (!DataManager.instance.m_IsNoAds)
        {
            ServicesManager.Ads().ShowBanner();
        }
    }

    // Update is called once per frame
    private void Update()
    {

        hintTxt.text = HintManager.Instance.Hints.ToString();

        if (GameManager.Instance.GameState == GameState.Playing)
        {
            SetCurrentNumber(GameManager.Instance.CurrentNumber);
            SetMoves(GameManager.Instance.Moves);
        }
        else if (GameManager.Instance.GameState == GameState.OutOfMoves && GameManager.Instance.IsError)
        {
            ShowErrorUI();
        }
        //UpdateMuteButtons();
    }


    ////////////////////////////Publish functions

    public void PlayButtonSound()
    {
        SoundManager.Instance.PlaySound(SoundManager.Instance.button);
    }

    //Gameplay buttons
    public void ShowHintBtn()
    {
        GameManager.Instance.ShowHintSteps();
        Analytics.instance.PushEventUseHint(GameManager.levelLoaded);
    }


    public void GetHitnsBtn()
    {
        getHintsBtn.SetActive(false);
        showHintsBtn.SetActive(true);
        getHintsPanel.SetActive(true);
        //UnityAdsManager.Instance.ShowRewardedVideoAd(0.5f);
    }

    public void CLRBtn()
    {

        StartCoroutine(IE_FlickerText());
        isLoadNewLvl = false;
        GameManager.Instance.LoadLevel();
        GameManager.Instance.ClearGame();
        if (winCoroutine != null)
        {
            StopCoroutine(winCoroutine);
        }
        WinText.SetActive(false);
    }

    public void LoadNextLevelBtl()
    {
        if (GameManager.levelLoaded == GameManager.Instance.TotalLevel())
        {
            GameManager.Instance.ShowPassAllLevel();
        }
        else
        {
            isLoadNewLvl = true;
            if (!DataManager.instance.m_IsNoAds)
            {
                Analytics.instance.PushEventRequestAds();
                ServicesManager.instance.ShowFullAds(() =>
                {
                    showHintsBtn.SetActive(true);
                    getHintsBtn.SetActive(true);
                    settingButtons.SetActive(true);
                    if (GameManager.levelLoaded < GameManager.Instance.TotalLevel())
                    {
                        GameManager.levelLoaded++;
                        GameManager.Instance.LoadLevel();
                        SetOperator();
                        GameManager.Instance.StartGame();
                        StopCoroutine(winCoroutine);
                        currentNumberTxt.gameObject.SetActive(true);
                        WinText.SetActive(false);
                        //for (int i = 0; i < arrayBtn.Length; i++)
                        //{
                        //    m_ArrBtnRect[i].anchoredPosition = m_ArrBtnRectPos[i] - new Vector3(0f, 11f);
                        //    m_ArrBtnShadow[i].effectDistance = new Vector2(0f, -2f);
                        //}
                    }
                });
            }
            else
            {
                showHintsBtn.SetActive(true);
                getHintsBtn.SetActive(true);
                settingButtons.SetActive(true);
                if (GameManager.levelLoaded < GameManager.Instance.TotalLevel())
                {
                    GameManager.levelLoaded++;
                    GameManager.Instance.LoadLevel();
                    SetOperator();
                    GameManager.Instance.StartGame();
                    StopCoroutine(winCoroutine);
                    currentNumberTxt.gameObject.SetActive(true);
                    WinText.SetActive(false);
                    //for (int i = 0; i < arrayBtn.Length; i++)
                    //{
                    //    m_ArrBtnRect[i].anchoredPosition = m_ArrBtnRectPos[i] - new Vector3(0f, 11f);
                    //    m_ArrBtnShadow[i].effectDistance = new Vector2(0f, -2f);
                    //}
                }
            }
        }
    }

    public void SettingBtn()
    {
        StartCoroutine(IE_PressSettingBtn());
    }



    //Setting buttons
    public void ShareBtn()
    {

    }

    public void LeaderboardBtn()
    {

    }

    public void AchivementBtn()
    {

    }

    public void RateAppBtn()
    {

    }

    public void PreviousLevelBtn()
    {
        if (GameManager.levelLoaded > 1)
        {
            isLoadNewLvl = true;
            GameManager.levelLoaded--;
            GameManager.Instance.LoadLevel();
            SetLevel(GameManager.Instance.LevelData.levelNumber);
            SetGoal(GameManager.Instance.LevelData.goal);
            SetMoves(GameManager.Instance.LevelData.listHintStep.Count);
            SetCurrentNumber(GameManager.Instance.LevelData.firstNumber);
            selectedLevelTxt.text = GameManager.levelLoaded.ToString();
        }
    }

    public void NextLevelBtn()
    {
        if (GameManager.levelLoaded < GameManager.Instance.TotalLevel())
        {
            if (GameManager.levelLoaded < GameManager.Instance.MaxLevelSolved() + 1)
            {
                isLoadNewLvl = true;
                GameManager.levelLoaded++;
                GameManager.Instance.LoadLevel();
                SetLevel(GameManager.Instance.LevelData.levelNumber);
                SetGoal(GameManager.Instance.LevelData.goal);
                SetMoves(GameManager.Instance.LevelData.listHintStep.Count);
                SetCurrentNumber(GameManager.Instance.LevelData.firstNumber);
                selectedLevelTxt.text = GameManager.levelLoaded.ToString();
            }
        }
    }

    public void PlayBtn()
    {
        isLoadNewLvl = true;
        StartCoroutine(IE_PressPlayBtn());
    }

    public void ToggleSound()
    {
        SoundManager.Instance.ToggleMute();
    }

    public void HandleRewardHintsAfterWatchAd(int hints, bool isFinished)
    {
        if (isFinished)
            StartCoroutine(RewardHints(hints, 0.5f));
        else
            getHintsPanel.SetActive(false);
    }

    /////////////////////////////Private functions
    void UpdateMuteButtons()
    {
        if (SoundManager.Instance.IsMuted())
        {
            soundOnBtn.gameObject.SetActive(false);
            soundOffBtn.gameObject.SetActive(true);
        }
        else
        {
            soundOnBtn.gameObject.SetActive(true);
            soundOffBtn.gameObject.SetActive(false);
        }
    }

    void ShowPrepareGameUI()
    {
        SetLevel(GameManager.Instance.LevelData.levelNumber);
        SetGoal(GameManager.Instance.LevelData.goal);
        SetMoves(GameManager.Instance.LevelData.listHintStep.Count);
        SetCurrentNumber(GameManager.Instance.LevelData.firstNumber);
        selectedLevelTxt.text = GameManager.levelLoaded.ToString();
        FaceEmotion.SetActive(false);
        currentNumberTxt.gameObject.SetActive(false);
        currentNumberAnim.enabled = false;
        operatorButtons.SetActive(false);
        gameplayButtons.SetActive(false);
        gameNameTxt.SetActive(true);
        PauseCanvas.SetActive(true);
        showHintsBtn.SetActive(true);
        getHintsBtn.SetActive(true);
        settingButtons.SetActive(true);
        FakeOperatorButton.SetActive(false);
        IngameAtt.SetActive(false);
        for (int i = 0; i < arrPauseBtn.Length; i++)
        {
            m_ArrPauseBtnRect[i].anchoredPosition = m_ArrPauseBtnRectPos[i] - new Vector3(0f, 11f);
            m_ArrPauseBtnShadow[i].enabled = true;
            m_ArrPauseBtnShadow[i].effectDistance = new Vector2(0f, -2f);
        }
        StartCoroutine(IE_LoadPrepareGameUI());
    }

    void ShowTutorialUI()
    {
        HideAll();
        TutorialCanvas.SetActive(true);
        currentNumberAnim.enabled = false;
        TutorialManager.Instance.ShowTut();
    }

    void ShowStartGameUI()
    {
        HideAll();
        PlayerCanvas.SetActive(true);
        IngameAtt.SetActive(true);
        operatorButtons.SetActive(true);
        gameplayButtons.SetActive(false);
        settingButtons.SetActive(true);
        gameNameTxt.SetActive(false);
        showHintsBtn.SetActive(true);
        getHintsBtn.SetActive(true);
        clearBtn.SetActive(true);
        nextBtn.SetActive(false);
        FaceEmotion.SetActive(true);
        PauseCanvas.SetActive(false);
        FakeOperatorButton.SetActive(true);
        currentNumberAnim.enabled = false;
        currentNumberTxt.gameObject.SetActive(true);
        DoneAllLevelCanvas.SetActive(false);
        HideAllFace();
        arrFace[0].SetActive(true);
        currentNumberTxt.color = new Color(currentNumberTxt.color.r, currentNumberTxt.color.g, currentNumberTxt.color.b, 1);
        SetLevel(GameManager.Instance.LevelData.levelNumber);
        SetGoal(GameManager.Instance.LevelData.goal);

        //Debug.Log(GameManager.Instance.IsSameLevel());
        if (!GameManager.Instance.IsSameLevel())
        {
            SetOperator();

        }
        if (isLoadNewLvl)
        {
            for (int i = 0; i < arrayBtn.Length; i++)
            {
                m_ArrBtnRect[i].anchoredPosition = m_ArrBtnRectPos[i] - new Vector3(0f, 11f);
                m_ArrBtnShadow[i].effectDistance = new Vector2(0f, -2f);
            }
            StartCoroutine(IE_ShowStartGameUI());
        }
        if (goalTxt.text.Length > 9)
        {
            goalTxt.fontSize = 15;
            movesTxt.fontSize = 15;
        }
        else
        {
            goalTxt.fontSize = 20;
            movesTxt.fontSize = 20;
        }

        Debug.Log("Lvl play " + GameManager.levelLoaded);
        GameManager.Instance.SetPreviousLevel();
        Analytics.instance.PushEventPlay(GameManager.levelLoaded);
        if (ServicesManager.AdsReward().IsAdsAvailable())
        {
            arrayBtn[3].gameObject.SetActive(true);
        }
        else
        {
            arrayBtn[3].gameObject.SetActive(false);
        }

        if (GameManager.levelLoaded < 4)
        {
            arrayBtn[0].gameObject.SetActive(false);
            arrayBtn[3].gameObject.SetActive(false);
        }

        for (int i = 0; i < arrayBtn.Length; i++)
        {
            if (arrayBtn[i].gameObject.activeSelf)
            {
                arrFakeBtn[i].SetActive(false);
            }
            else
            {
                arrFakeBtn[i].SetActive(true);
            }
        }

        if (checkLength())
        {
            for (int i = 0; i < arrayBtn.Length; i++)
            {
                arrayBtn[i].operatorText.fontSize = 32;
            }
        }
        else
        {
            for (int i = 0; i < arrayBtn.Length; i++)
            {
                arrayBtn[i].operatorText.fontSize = 50;
            }
        }

        for (int i = 0; i < opeScript.Length; i++)
        {
            opeScript[i].enabled = true;
        }
        if (winCoroutine != null)
        {
            StopCoroutine(winCoroutine);
        }
        WinText.SetActive(false);
    }

    void HideAllFace()
    {
        for (int i = 0; i < arrFace.Length; i++)
        {
            arrFace[i].SetActive(false);
        }
    }

    void SetOperator()
    {
        foreach (OperatorButtonController o in arrayBtn)
        {
            o.SetPlayingUI();
            if (o.GetOpeCount() == 2)
            {
                o.SetColorBtn1();
            }
            else if (o.GetOpeCount() == 5)
            {
                o.SetColorBtn2();
            }
            else if (o.GetOpeCount() == 6)
            {
                o.SetColorBtn3();
            }
            else if (o.GetOpeCount() == 8)
            {
                o.SetColorBtn4();
            }
            else if (o.GetOpeCount() == 9)
            {
                o.SetColorBtn5();
            }

            //o.SetBlurBtn();
            //if (GameManager.Instance.GameState == GameState.PassLevel)
            //{
            // o.ResetBlurBtn();
            // }
        }

    }


    bool checkLength()
    {
        bool isShort = false;
        for (int i = 0; i < arrayBtn.Length; i++)
        {
            if (arrayBtn[i].operatorText.text.Length >= 6)
            {
                isShort = true;
            }
        }
        return isShort;
    }

    void ShowOutOfMovesUI()
    {
        var camShake = operatorButtons.GetComponent<CameraShake>();
        var camShake1 = GamePlayUI.GetComponent<CameraShake>();
        var camShake2 = FakeOperatorButton.GetComponent<CameraShake>();
        camShake.StartShake();
        camShake1.StartShake();
        camShake2.StartShake();
        HideAllFace();
        Debug.Log("");
        arrFace[Random.Range(1, 3)].SetActive(true);
        //currentNumberTxt.text = GameManager.Instance.outOfMovesText;
        //showHintsBtn.SetActive(false);
        // getHintsBtn.SetActive(false);
        //  BlackBtnPos1.SetActive(true);
        // BlackBtnPos4.SetActive(true);
        // BlackBtnPos7.SetActive(true);
        //settingButtons.SetActive(false);
        Analytics.instance.PushEventDead(GameManager.levelLoaded);
    }

    void ShowPassLevelUI()
    {
        HideAllFace();
        arrFace[3].SetActive(true);
        for (int i = 0; i < arrFakeBtn.Length; i++)
        {
            arrFakeBtn[i].SetActive(true);
        }
        for (int i = 0; i < opeScript.Length; i++)
        {
            opeScript[i].enabled = false;
        }
        winCoroutine = StartCoroutine(IE_SwitchGoalTxt());
        StartCoroutine(IE_ShowPassLevel());
    }

    public void SetLevel(int level)
    {
        levelTxt.text = "LEVEL: " + level.ToString();
    }

    public void SetGoal(int goal)
    {
        goalTxt.text = "GOAL: " + goal.ToString();
    }

    public void SetMoves(int moves)
    {
        movesTxt.text = "MOVES: " + moves.ToString();
    }
    void SetCurrentNumber(int number)
    {
        currentNumberTxt.text = number.ToString();
    }

    IEnumerator RewardHints(int hints, float delay)
    {
        yield return new WaitForSeconds(delay);
        for (int i = 0; i < hints; i++)
        {
            HintManager.Instance.AddHints(1);
            SoundManager.Instance.PlaySound(SoundManager.Instance.addHints);
            yield return new WaitForSeconds(0.08f);
        }
        SoundManager.Instance.PlaySound(SoundManager.Instance.rewarded);
        yield return new WaitForSeconds(3f);
        getHintsPanel.SetActive(false);
    }
    public void ShowBuyHintCanvas()
    {
        BuyHintRect.ZKanchoredPositionTo(new Vector3(0f, 0f)).setEaseType(EaseType.BackIn).start();
    }

    public void OnClickExitBuyHint()
    {
        BuyHintRect.ZKanchoredPositionTo(BuyHintRectPos).setEaseType(EaseType.BackOut).start();
    }

    public void OnClickExitFinishCanvas()
    {
        DoneAllLevelCanvas.SetActive(false);
        if (winCoroutine != null)
        {
            StopCoroutine(winCoroutine);
        }
        ShowStartGameUI();
        StartCoroutine(IE_ShowStartGameUI());
    }

    public void OnClickBuyHintTier1()
    {
        ServicesManager.IAP().PurchaseProduct("com.DTA.Calculator.3hints", (result) =>
        {
            if (result)
            {
                HintManager.Instance.AddHints(3);
            }
            else
            {
                HintManager.Instance.AddHints(0);
            }
        });
    }

    public void OnClickBuyHintTier2()
    {
        ServicesManager.IAP().PurchaseProduct("com.DTA.Calculator.9hints", (result) =>
        {
            if (result)
            {
                HintManager.Instance.AddHints(9);
                DataManager.Instance.m_IsNoAds = true;
                ServicesManager.Ads().HideBanner();
                DataManager.instance.SaveData();
            }
            else
            {
                HintManager.Instance.AddHints(0);
            }
        });
    }

    public void OnClickBuyHintTier3()
    {
        ServicesManager.IAP().PurchaseProduct("com.DTA.Calculator.16hints", (result) =>
        {
            if (result)
            {
                HintManager.Instance.AddHints(16);
                DataManager.Instance.m_IsNoAds = true;
                ServicesManager.Ads().HideBanner();
                DataManager.instance.SaveData();
            }
            else
            {
                HintManager.Instance.AddHints(0);
            }
        });
    }

    public void OnClickBuyHintTier4()
    {
        ServicesManager.IAP().PurchaseProduct("com.DTA.Calculator.36hints", (result) =>
        {
            if (result)
            {
                HintManager.Instance.AddHints(36);
                DataManager.Instance.m_IsNoAds = true;
                ServicesManager.Ads().HideBanner();
                DataManager.instance.SaveData();
            }
            else
            {
                HintManager.Instance.AddHints(0);
            }
        });
    }

    public void OnClickBuyHintTier5()
    {
        ServicesManager.IAP().PurchaseProduct("com.DTA.Calculator.80hints", (result) =>
        {
            if (result)
            {
                HintManager.Instance.AddHints(80);
                DataManager.Instance.m_IsNoAds = true;
                ServicesManager.Ads().HideBanner();
                DataManager.instance.SaveData();
            }
            else
            {
                HintManager.Instance.AddHints(0);
            }
        });
    }

    public void OnClickDailyHint()
    {
        Analytics.instance.PushEventDailyHint(GameManager.levelLoaded);
        ServicesManager.AdsReward().ShowAds(() =>
        {
            HintManager.Instance.AddHints(1);
        });
    }

    void HideAll()
    {
        TutorialCanvas.SetActive(false);
        PlayerCanvas.SetActive(false);
    }

    IEnumerator IE_FlickerText(System.Action callback = null)
    {
        movesTxt.gameObject.SetActive(false);
        currentNumberTxt.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.25f);
        movesTxt.gameObject.SetActive(true);
        currentNumberTxt.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.25f);
        currentNumberTxt.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.25f);
        currentNumberTxt.gameObject.SetActive(true);
    }

    IEnumerator IE_SwitchGoalTxt()
    {
        currentNumberTxt.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.25f);
        currentNumberTxt.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.25f);
        currentNumberTxt.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.25f);
        currentNumberTxt.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.25f);
        while (isWin)
        {
            WinText.SetActive(true);
            currentNumberTxt.gameObject.SetActive(false);
            yield return new WaitForSeconds(1f);
            WinText.SetActive(false);
            currentNumberTxt.gameObject.SetActive(true);
            yield return new WaitForSeconds(1f);
        }
    }

    IEnumerator IE_ShowPassLevel()
    {
        m_ArrBtnRect[0].ZKanchoredPositionTo(m_ArrBtnRectPos[0] - new Vector3(0f, 11f), 0.1f).start();
        m_ArrBtnRect[3].ZKanchoredPositionTo(m_ArrBtnRectPos[3] - new Vector3(0f, 11f), 0.1f).start();
        m_ArrBtnRect[6].ZKanchoredPositionTo(m_ArrBtnRectPos[6] - new Vector3(0f, 11f), 0.1f).start();
        m_ArrBtnRect[2].ZKanchoredPositionTo(m_ArrBtnRectPos[2] - new Vector3(0f, 11f), 0.1f).start();
        yield return new WaitForSeconds(0.1f);
        m_ArrBtnShadow[0].enabled = false;
        m_ArrBtnShadow[2].enabled = false;
        m_ArrBtnShadow[3].enabled = false;
        m_ArrBtnShadow[6].enabled = false;
        yield return new WaitForSeconds(0.1f);
        arrayBtn[0].gameObject.SetActive(false);
        arrayBtn[2].gameObject.SetActive(false);
        arrayBtn[3].gameObject.SetActive(false);
        arrayBtn[6].gameObject.SetActive(false);
        nextBtn.SetActive(true);
    }

    IEnumerator IE_ShowNextLevel()
    {
        for (int i = 0; i < arrayBtn.Length; i++)
        {
            m_ArrBtnRect[i].ZKanchoredPositionTo(m_ArrBtnRectPos[i], 0.1f).start();
            m_ArrBtnShadow[i].enabled = true;
        }

        yield return new WaitForSeconds(0.1f);
        for (int i = 0; i < arrayBtn.Length; i++)
        {
            m_ArrBtnShadow[i].effectDistance = new Vector2(0f, -12f);
        }
    }

    public void ShowErrorUI()
    {
        currentNumberTxt.text = "ERROR";
    }

    IEnumerator IE_LoadPrepareGameUI()
    {

        for (int i = 0; i < arrPauseBtn.Length; i++)
        {
            m_ArrPauseBtnRect[i].ZKanchoredPositionTo(m_ArrPauseBtnRectPos[i], 0.1f).start();
        }

        yield return new WaitForSeconds(0.1f);
        for (int i = 0; i < arrPauseBtn.Length; i++)
        {
            m_ArrPauseBtnShadow[i].effectDistance = new Vector2(0f, -12f);
        }
    }

    IEnumerator IE_PressSettingBtn()
    {
        for (int i = 0; i < arrayBtn.Length; i++)
        {
            m_ArrBtnRect[i].ZKanchoredPositionTo(m_ArrBtnRectPos[i] - new Vector3(0f, 11f), 0.1f).start();
        }
        yield return new WaitForSeconds(0.1f);
        for (int i = 0; i < arrayBtn.Length; i++)
        {
            m_ArrBtnShadow[i].effectDistance = new Vector2(0f, -2f);
        }
        yield return new WaitForSeconds(0.1f);
        GameManager.Instance.PrepareGame();
    }

    IEnumerator IE_PressPlayBtn()
    {
        for (int i = 0; i < arrPauseBtn.Length; i++)
        {
            m_ArrPauseBtnRect[i].ZKanchoredPositionTo(m_ArrPauseBtnRectPos[i] - new Vector3(0f, 11f), 0.1f).start();
        }
        yield return new WaitForSeconds(0.1f);
        for (int i = 0; i < arrPauseBtn.Length; i++)
        {
            m_ArrPauseBtnShadow[i].effectDistance = new Vector2(0f, -2f);
        }
        yield return new WaitForSeconds(0.1f);
        GameManager.Instance.StartGame();
    }

    IEnumerator IE_ShowStartGameUI()
    {
        for (int i = 0; i < arrayBtn.Length; i++)
        {
            m_ArrBtnRect[i].anchoredPosition = m_ArrBtnRectPos[i] - new Vector3(0f, 10f);
        }
        yield return new WaitForSeconds(0.1f);
        for (int i = 0; i < arrayBtn.Length; i++)
        {
            m_ArrBtnRect[i].ZKanchoredPositionTo(m_ArrBtnRectPos[i], 0.1f).start();
            m_ArrBtnShadow[i].enabled = true;
            m_ArrBtnShadow[i].effectDistance = new Vector2(0f, -12f);
        }
    }

    public void SetTutorialText(string text)
    {
        TutorialText.text = text;
    }

    IEnumerator IE_CalculateText()
    {
        var color = currentNumberTxt.color;
        color.a = 0f;
        currentNumberTxt.color = color;
        yield return new WaitForSeconds(0.1f);
        color.a = 1f;
        currentNumberTxt.color = color;
    }

    public void StartCalculateText()
    {
        StartCoroutine(IE_CalculateText());
    }

    public void ShowDoneAllLevelUI()
    {
        DoneAllLevelCanvas.SetActive(true);
        DoneAllLevelText.text = "More levels coming soon. Stay tuned!";
    }
}