﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calculator : MonoBehaviour
{
    public static int storeNumber = -1;
    public static int StoreButton(int currentNumber, int a)
    {
        if (storeNumber > -1)
        {
            return InsertToTheLast(currentNumber, storeNumber);
        }
        else
        {
            storeNumber = a;
            return currentNumber;
        }
    }

    /// <summary>
    /// Return a+b. 
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns> a+b </returns>
    public static int Plus(int a, int b)
    {
        return a + b;
    }

    /// <summary>
    /// Return a-b. 
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns> a-b </returns>
    /// Phép trừ
    public static int Substract(int a, int b)
    {
        return a - b;
    }

    /// <summary>
    /// Return a*b. 
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns> a*b </returns>
    /// Phép nhân
    public static int Multiple(int a, int b)
    {
        return a * b;
    }

    /// <summary>
    /// Return a/b. 
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns> a/b </returns>
    /// Phép chia
    public static float Divide(int a, int b)
    {

        return a * 1.0f / b;
    }

    /// <summary>
    /// Devide the current number for the given number and return the surplus number.
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns> a%b </returns>
    /// Phép lấy phần dư
    public static int Surplus(int a, int b)
    {
        return a % b;
    }

    /// <summary>
    /// Increase all number in the string with the given value. Example:
    /// Current number: 1234
    /// Operator: 1
    /// Result: 2345
    /// </summary>
    /// <param name="currentNumber"> The current number </param>
    /// <param name="increasedValue"> The number which is used to increase all numbers of the current number </param>
    /// <returns> The new number after increasing </returns>
    /// cộng tát cả các chữ số với 1 số nào đó
    public static int IncreaseNum(int currentNumber, int increasedValue)
    {
        string s = Math.Abs(currentNumber).ToString();
        string[] rs = new string[s.Length];

        for (int i = 0; i < s.Length; i++)
        {
            rs[i] = (int.Parse(s[i].ToString()) + increasedValue).ToString();
        }

        return currentNumber > 0 ? (int.Parse(string.Join("", rs))) : -(int.Parse(string.Join("", rs)));

    }

    /// <summary>
    /// Decrease all number in the string with the given value. Example:
    /// Current number: 1234
    /// Operator: ↓1
    /// Result: 123
    /// </summary>
    /// <param name="currentNumber"> The current number </param>
    /// <param name="decreasedValue"> The number which is used to decrease all numbers of the current number </param>
    /// <returns> The new number after decreasing </returns>
    /// Trừ tất cả các sô cho 1 chữ số nào đó
    public static int DecreaseNum(int currentNumber, int decreasedValue)
    {
        string s = Math.Abs(currentNumber).ToString();
        string[] rs = new string[s.Length];

        for (int i = 0; i < s.Length; i++)
        {
            rs[i] = ((int.Parse(s[i].ToString()) - decreasedValue) > 0 ? (int.Parse(s[i].ToString()) - decreasedValue) : 0).ToString();
        }

        return currentNumber > 0 ? (int.Parse(string.Join("", rs))) : -(int.Parse(string.Join("", rs)));
    }

    /// <summary>
    /// Remove the last number of the current string. Example:
    /// Current number: 12345
    /// Operator: <<
    /// Result: 1234
    /// </summary>
    /// <param name="currentNumber"> The current number </param>
    /// <returns> The new number after we remove its last number </returns>
    /// Bỏ số cuối cùng
    public static int RemoveLastNum(int currentNumber)
    {
        string s = currentNumber.ToString().Substring(0, currentNumber.ToString().Length - 1);
        if (string.IsNullOrEmpty(s))
            return 0;
        else
        {
            int t;
            if (!int.TryParse(s, out t))
            {
                return 0;
            }
            return t;
        }
    }

    /// <summary>
    /// Remove the first number of the current string. Example:
    /// Current number: 12345
    /// Operator: >>
    /// Result: 2345
    /// </summary>
    /// <param name="currentNumber"> The current number </param>
    /// <returns> The new number after we remove its first number </returns>
    /// Bỏ chữ số đầu tiên
    public static int RemoveFirstNum(int currentNumber)
    {
        string s = Math.Abs(currentNumber).ToString().Substring(1);
        if (string.IsNullOrEmpty(s))
            return 0;
        else
            return currentNumber > 0 ? (int.Parse(s)) : -(int.Parse(s));
    }

    /// <summary>
    /// Insert a number to the last of the string. Example: 
    /// Current number: 1234
    /// Operator: +Last_5
    /// Result: 12345
    /// </summary>
    /// <param name="currentNumber"> The current number </param>
    /// <param name="insertedNumber"> The number we want insert to the last </param>
    /// <returns> The new number after inserted </returns>
    /// Thêm số vào cuối
    public static int InsertToTheLast(int currentNumber, int insertedNumber)
    {
        return int.Parse(currentNumber.ToString() + insertedNumber);
    }

    /// <summary>
    /// Insert a number to the first of the string. Example: 
    /// Current number: 1234
    /// Operator: +First_5
    /// Result: 51234
    /// </summary>
    /// <param name="currentNumber"> The current number </param>
    /// <param name="insertedNumber"> The number we want insert to the first </param>
    /// <returns> The new number after inserted </returns>
    /// Thêm 1 số vào đầu
    private static int InsertToTheFirst(int currentNumber, int insertedNumber)
    {
        string s = insertedNumber + Math.Abs(currentNumber).ToString();
        return currentNumber > 0 ? (int.Parse(s)) : -(int.Parse(s));
    }

    /// <summary>
    /// Convert the number from x to y. Example:
    /// Current number: 123
    /// Operator: 1=>2
    /// Result: 223
    /// </summary>
    /// <param name="currentNumber">The current number</param>
    /// <param name="numberFrom">The number of the current number needed to convert</param>
    /// <param name="numberTo">The number which we use it to convert the current number</param>
    /// <returns> The new number after converted </returns>
    /// Chuyển đổi số này sang số khác
    private static int ConvertNums(int currentNumber, int numberFrom, int numberTo)
    {
        return int.Parse(currentNumber.ToString().Replace(numberFrom.ToString(), numberTo.ToString()));
    }

    /// <summary>
    /// Duplicate the current number with a given number. Example:
    /// Current number: 2
    /// Operator: ^3
    /// Result: 8
    /// </summary>
    /// <param name="a"> The current number </param>
    /// <param name="b"> The number of times we want to duplicate </param>
    /// <returns> a^b </returns>
    /// Hàm mũ 
    private static int Duplicate(int a, int b)
    {
        return (int)Math.Pow(a, b);
    }

    /// <summary>
    /// Converts negative numbers and positive numbers
    /// Example: 
    /// Current number: -1
    /// Operator: +<=>-
    /// Result: 1
    /// </summary>
    /// <param name="a"> The current number </param>
    /// <returns> The new number that is opposed to the current number </returns>
    /// Đảo dấu
    public static int Opposite(int a)
    {
        return -a;
    }

    /// <summary>
    /// Reverse the curren number. Example: 
    /// Current number: 123
    /// Opertor: Reverse
    /// Result: 321
    /// </summary>
    /// <param name="a"> The current number </param>
    /// <returns> new number after being reversed </returns>
    /// Đảo ngước các chữ số
    public static int Reverse(int a)
    {
        int n1 = Math.Abs(a);
        int n2 = n1 % 10;
        while ((n1 /= 10) > 0)
        {
            n2 = n2 * 10 + n1 % 10;
        }
        return a > 0 ? n2 : -n2;
    }

    /// <summary>
    /// Sum all the single number of the string. Example:
    /// Current number: 123
    /// Operator: Sum
    /// Result: 6
    /// </summary>
    /// <param name="currentNumber"> The current number </param>
    /// <returns> Sum of all the numbers of the current number </returns>
    /// Tổng các chữ số
    public static int Sum(int currentNumber)
    {
        int temp = Math.Abs(currentNumber);
        int kq = 0;
        int n;

        while (temp > 0)
        {
            n = temp % 10;
            kq += n;
            temp /= 10;
        }

        return kq;
    }

    /// <summary>
    /// Move the last left digit of the current number to the right. Example:
    /// Current number: 1234
    /// Operator: <Shift
    /// Result: 2341
    /// </summary>
    /// <param name="currentNumber"> The current number </param>
    /// <returns> The new number after being moved </returns>
    /// Dịch các chữ số theo vòng sang trái
    public static int ShiftLeft(int currentNumber)
    {
        string s = Math.Abs(currentNumber).ToString();
        string temp = s[0].ToString();

        return currentNumber > 0 ? (int.Parse(s.Substring(1, s.Length - 1) + temp)) : -(int.Parse(s.Substring(1, s.Length - 1) + temp));

    }

    /// <summary>
    /// Move the last right digit of the current number to the left. Example:
    /// Current number: 1234
    /// Operator: Shift>
    /// Result: 4123
    /// <param name="currentNumber"></param>
    /// <returns> The new number after being moved </returns>
    /// Dịch vòng phải
    public static int ShiftRight(int currentNumber)
    {
        string s = Math.Abs(currentNumber).ToString();
        string temp = s[s.Length - 1].ToString();

        return currentNumber > 0 ? (int.Parse(temp + s.Substring(0, s.Length - 1))) : -(int.Parse(temp + s.Substring(0, s.Length - 1)));

    }

    /// <summary>
    /// Mirror the current number. Example:
    /// Current number: 12
    /// Operator: Mirror
    /// Result: 1221
    /// </summary>
    /// <param name="currentNumber"> The current number </param>
    /// <returns> The new number after mirror </returns>
    /// Phép lấy gương
    public static int Mirror(int currentNumber)
    {
        string re = Reverse(Math.Abs(currentNumber)).ToString().Length < Math.Abs(currentNumber).ToString().Length ? 0 + Reverse(Math.Abs(currentNumber)).ToString() : Reverse(Math.Abs(currentNumber)).ToString();
        return int.Parse(currentNumber.ToString() + re);
    }


    /// <summary>
    /// Remove a given number if existed in the string. Example:
    /// Current number: 12345
    /// Operator: <23>
    /// Result: 145
    /// </summary>
    /// <param name="currentNumber"> The current number </param>
    /// <param name="removedNumber"> The number needed to remove </param>
    /// <returns> The new number after remove </returns>
    /// xóa các chữ số đầu vào có trong chuỗi
    public static int RemoveNum(int currentNumber, int removedNumber)
    {
        string s = currentNumber.ToString().Replace(removedNumber.ToString(), "");
        return int.Parse(s);
    }


    /// <summary>
    /// Calculate the goal with the given first number and the list of operators
    /// </summary>
    /// <param name="firstNum"></param>
    /// <param name="listOperator"></param>
    /// <returns></returns>
    public static int CalculateGoal(out bool isError)
    {
        isError = false;
        int result = LevelManager.levelData.firstNumber;
        float? floatResult = null;
        for (int i = 0; i < LevelManager.levelData.listHintStep.Count; i++)
        {
            result = CalculateSingleOperator(result, LevelManager.levelData.listHintStep[i], out floatResult);
            if (floatResult != null)
            {
                if (floatResult.Value - result != 0)
                {
                    isError = true;
                }
            }
            if(result.ToString().Length >6)
            {
                isError = true;
            }
        }
        
        return result;
    }

    public static string BetterSolution { get; private set; }

    class GoodSolutionResult
    {
        internal int firstNumber;
        internal string[] listHint;
        internal int steps;
        internal int goal;
        internal bool isHaveBetterSolution;
    }

    static GoodSolutionResult lastResult = null;

    static bool IsSameListString(string[] listA, string[] listB)
    {
        if (listA == null && listB == null) return true;
        if (listA == null || listB == null) return false;
        if (listA.Length != listB.Length) return false;

        for(int i = 0; i < listA.Length; i++)
        {
            if (listA[i] != listB[i]) return false;
        }

        return true;
    }

    public static bool IsHaveBetterSolution(int firstNumber, string[] listHint, int steps, int goal)
    {
        if (listHint == null || listHint.Length == 0)
        {
            return false;
        }

        if (firstNumber == goal)
        {
            BetterSolution = STR_SHORTER_SOLUTION + "firstNumber = goal";
            return true;
        }

        if(lastResult != null)
        {
            if ((lastResult.firstNumber == firstNumber)
                && (lastResult.steps == steps)
                && (lastResult.goal == goal)
                && (IsSameListString(lastResult.listHint, listHint))
                )
            {
                return lastResult.isHaveBetterSolution;
            }
        }

        EstimateAllSolutions(listHint.Length, steps);

        lastResult = new GoodSolutionResult();
        lastResult.firstNumber = firstNumber;
        lastResult.steps = steps;
        lastResult.goal = goal;
        lastResult.listHint = listHint;

        var betterMaxSteps = steps - 1;
        var listHintStep = new string[steps];
        
        if (TryWithHintStep(0, listHintStep,
            firstNumber, listHint, betterMaxSteps, goal)) return true;

        var listCountUse = new int[listHint.Length];

        for(int i = 0; i < listCountUse.Length; i++)
        {
            listCountUse[i] = 0;
        }

        return TryWithHintMaxStep(0, listHintStep, listCountUse,
            firstNumber, listHint, steps, goal);
    }

    public static int CountAllCases { get; private set; }

    static void EstimateAllSolutions(int operatorsNum, int stepsNum)
    {
        int[] countUsed = new int[operatorsNum];

        for(int i = 0; i < countUsed.Length; i++)
        {
            countUsed[i] = 0;
        }

        CountAllCases = 0;
        TryWithOperator(0, operatorsNum, stepsNum, countUsed);
    }

    static void TryWithOperator(int stepID, int operatorsNum, int stepsNum, 
        int[] countUsed)
    {
        if(stepID >= stepsNum)
        {
            CountAllCases += 1;
            return;
        }

        int nonUsedNum = CountAllZero(countUsed);

        int remainSteps = stepsNum - stepID;

        if (remainSteps < nonUsedNum) return;

        for(int i = 0; i < operatorsNum; i++)
        {
            if(countUsed[i] > 0 && remainSteps <= nonUsedNum)
            {
                continue;
            }

            countUsed[i] += 1;

            TryWithOperator(stepID + 1, operatorsNum, stepsNum, countUsed);

            countUsed[i] -= 1;
        }
    }

    static bool TryWithHintStep(int stepID, string[] listHintStep,
        int currentResult, string[] listHint, int steps, int goal)
    {
        if (stepID >= steps)
        {
            lastResult.isHaveBetterSolution = false;
            return false;
        }
        float? floatResult = null;
        for (int i = 0; i < listHint.Length; i++)
        {
            listHintStep[stepID] = listHint[i];
            int newResult = CalculateSingleOperator(currentResult, listHint[i], out floatResult);

            //Debug.Log(LogShorterSolution(stepID, listHintStep));
            //Debug.Log("tempResult: " + newResult);

            if (floatResult != null)
            {
                //Debug.Log("tempFloatResult: " + floatResult.Value);
                floatResult = null;
                continue;
            }

            if (newResult == goal)
            {
                BetterSolution = LogShorterSolution(stepID, listHintStep);
                lastResult.isHaveBetterSolution = true;
                return true;
            }

            if (TryWithHintStep(stepID + 1, listHintStep,
                newResult, listHint, steps, goal))
            {
                return true;
            }
        }

        lastResult.isHaveBetterSolution = false;
        return false;
    }

    static int CountAllZero(int[] listInt)
    {
        if (listInt == null || listInt.Length == 0) return 0;

        int count = 0;
        for (int i = 0; i < listInt.Length; i++)
        {
            if (listInt[i] == 0) count += 1;
        }
        return count;
    }

    static bool IsHaveZero(int[] listInt)
    {
        if (listInt == null || listInt.Length == 0) return false;
        for(int i = 0; i < listInt.Length; i++)
        {
            if (listInt[i] == 0)
                return true;
        }
        return false;
    }

    static bool TryWithHintMaxStep(int stepID, string[] listHintStep, int[] listCountUse,
        int currentResult, string[] listHint, int steps, int goal)
    {
        if (stepID >= steps)
        {
            if (currentResult == goal && IsHaveZero(listCountUse))
            {
                BetterSolution = LogBetterSolution(stepID - 1, listHintStep);
                lastResult.isHaveBetterSolution = true;
                return true;
            }

            lastResult.isHaveBetterSolution = false;
            return false;
        }
        float? floatResult = null;
        for (int i = 0; i < listHint.Length; i++)
        {
            listHintStep[stepID] = listHint[i];
            listCountUse[i] += 1;
            int newResult = CalculateSingleOperator(currentResult, listHint[i], out floatResult);

            if (floatResult != null)
            {
                floatResult = null;
                continue;
            }

            if (TryWithHintMaxStep(stepID + 1, listHintStep, listCountUse,
                newResult, listHint, steps, goal))
            {
                return true;
            }

            listCountUse[i] -= 1;
        }

        lastResult.isHaveBetterSolution = false;
        return false;
    }

    const string STR_BETTER_SOLUTION = "Better Solution: ";
    const string STR_SHORTER_SOLUTION = "Shorter Solution: ";
    const string STR_NEXT_STEP = " -> ";

    static string LogBetterSolution(int lastID, string[] listHintStep)
    {
        var solution = new System.Text.StringBuilder();
        solution.Append(STR_BETTER_SOLUTION);
        for (int i = 0; i <= lastID; i++)
        {
            solution.Append(listHintStep[i]);
            if (i < lastID)
            {
                solution.Append(STR_NEXT_STEP);
            }
        }
        return solution.ToString();
    }

    static string LogShorterSolution(int lastID, string[] listHintStep)
    {
        var solution = new System.Text.StringBuilder();
        solution.Append(STR_SHORTER_SOLUTION);
        for (int i = 0; i <= lastID; i++)
        {
            solution.Append(listHintStep[i]);
            if (i < lastID)
            {
                solution.Append(STR_NEXT_STEP);
            }
        }
        return solution.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    ///     Input: abc 
    ///     Output: (10-a)(10-b)(10-c)
    /// Example: 123 -> 987
    /// <param name="firstNum"></param>
    /// <param name="listOperator"></param>
    /// <returns></returns>
    public static int TenSubstractDigit(int currentNumber)
    {
        int temp = Math.Abs(currentNumber);
        string kq = "";
        int n;

        while (temp > 0)
        {
            n = temp % 10;
            n = 10 - n;
            kq += n;
            temp /= 10;
        }
        int kqNum = int.Parse(kq);
        kqNum = Reverse(kqNum);
        return kqNum;
    }

    public static int Portals(int currentNumber, int inPort, int outPort)
    {
        int portNumber = currentNumber / (int)Math.Pow(10, inPort);
        int portResult = 0;
        if (portNumber > 0)
        {
            currentNumber = currentNumber % (int)Math.Pow(10, inPort);
            portResult = (int)Math.Pow(10, outPort);
            return (currentNumber + portResult);
        }

        return currentNumber;
    }

    /// <summary>
    /// Calculate single operator
    /// </summary>
    /// <param name="currentNumber"></param>
    /// <param name="ope"></param>
    /// <returns></returns>

    public static int CalculateSingleOperator(int currentNumber, string ope, out float? floatResult)
    {
        int result = currentNumber;
        floatResult = null;
        string opeRemoveNums = RegexHandler.RemovedNums(ope);
        List<string> listNums = RegexHandler.RemovedChars(ope);
        switch (opeRemoveNums)
        {
            case OperatorTypes.Plus:
                result = Plus(result, int.Parse(listNums[0]));
                break;
            case OperatorTypes.Subtract:
                result = Substract(result, int.Parse(listNums[0]));
                break;
            case OperatorTypes.Multiple:
                result = Multiple(result, int.Parse(listNums[0]));
                break;
            case OperatorTypes.Devide:
                floatResult = Divide(result, int.Parse(listNums[0]));
                result = (int)floatResult;
                if(floatResult == result)
                {
                    floatResult = null;
                }
                break;
            case OperatorTypes.Surplus:
                result = Surplus(result, int.Parse(listNums[0]));
                break;
            case OperatorTypes.DecreaseNum:
                result = DecreaseNum(result, int.Parse(listNums[0]));
                break;
            case OperatorTypes.IncreaseNum:
                result = IncreaseNum(result, int.Parse(listNums[0]));
                break;
            case OperatorTypes.RemoveLastNum:
                result = RemoveLastNum(result);
                break;
            case OperatorTypes.RemoveFirstNum:
                result = RemoveFirstNum(result);
                break;
            case OperatorTypes.InsertToTheLast:
                result = InsertToTheLast(result, int.Parse(listNums[0]));
                break;
            case OperatorTypes.InsertToTheFirst:
                result = InsertToTheFirst(result, int.Parse(listNums[0]));
                break;
            case OperatorTypes.ConvertNums:
                result = ConvertNums(result, int.Parse(listNums[0]), int.Parse(listNums[1]));
                break;
            case OperatorTypes.Duplicate:
                result = Duplicate(result, int.Parse(listNums[0]));
                break;
            case OperatorTypes.Opposite:
                result = Opposite(result);
                break;
            case OperatorTypes.Reverse:
                result = Reverse(result);
                break;
            case OperatorTypes.Sum:
                result = Sum(result);
                break;
            case OperatorTypes.ShiftLeft:
                result = ShiftLeft(result);
                break;
            case OperatorTypes.ShiftRight:
                result = ShiftRight(result);
                break;
            case OperatorTypes.Mirror:
                result = Mirror(result);
                break;
            case OperatorTypes.RemoveNum:
                result = RemoveNum(result, int.Parse(listNums[0]));
                break;
            case OperatorTypes.Portals:
                result = Portals(result, int.Parse(listNums[0]), int.Parse(listNums[1]));
                break;
            case OperatorTypes.TenSubstractDigit:
                result = TenSubstractDigit(result);
                break;
            case OperatorTypes.StoreResult:
                result = StoreButton(result, int.Parse(listNums[0]));
                break;
            case OperatorTypes.ChangeValueButtonPlus2:
                result = result + 2;
                break;
            default:
                break;
        }
        return result;
    }
}
