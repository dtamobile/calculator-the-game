﻿

namespace Root
{

    public interface IDebugLogService
    {

        void Log(object obj);

        void Error(object obj);

    }

}

