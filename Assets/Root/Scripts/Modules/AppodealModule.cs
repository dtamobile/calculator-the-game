﻿//need to define if using AppodealAds
//#define APPODEAL_ADS

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if (APPODEAL_ADS)
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
#endif

namespace Root
{
#if (APPODEAL_ADS)
    public class AppodealModule : IAdsService, IRewardedVideoAdListener, IInterstitialAdListener, IBannerAdListener
    {
        Action m_CloseCallback;
        Action m_CloseRewardCallback;

        bool isNeedToShowBannerAds = false;

        public void Init(AdsConfig adsConfig)
        {

        }

        public void InitAppodeal(AdsConfig adsConfig)
        {
#if UNITY_ANDROID
            string appKey = adsConfig.AndroidAppId;
#elif UNITY_IOS
            string appKey = adsConfig.IosAppId;
#else
            string appKey = adsConfig.AndroidAppId;
#endif
            //string appKey = "fee50c333ff3825fd6ad6d38cff78154de3025546d47a84f";
            Appodeal.disableNetwork("applovin");
            Appodeal.disableNetwork("chartboost");
            Appodeal.disableNetwork("unity_ads");
            Appodeal.disableNetwork("openx");
            Appodeal.disableNetwork("nexage");
            Appodeal.disableNetwork("tapsense");
            Appodeal.disableNetwork("mailru");
            Appodeal.disableNetwork("inmobi");
            Appodeal.disableNetwork("vungle");
            Appodeal.disableNetwork("startapp");
            Appodeal.disableNetwork("yandex");
            Appodeal.disableNetwork("appnexus");
            Appodeal.disableNetwork("liverail");
            Appodeal.disableNetwork("tapjoy");
            Appodeal.disableNetwork("cheetah");
            Appodeal.disableNetwork("revmob");
            Appodeal.setAutoCache(Appodeal.INTERSTITIAL, true);
            Appodeal.setAutoCache(Appodeal.BANNER, true);
            Appodeal.setAutoCache(Appodeal.REWARDED_VIDEO, true);
            Appodeal.disableLocationPermissionCheck();
            Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER | Appodeal.REWARDED_VIDEO);
            Appodeal.setInterstitialCallbacks(this);
            Appodeal.setRewardedVideoCallbacks(this);
            Appodeal.setBannerCallbacks(this);
        }
        public void ShowFullAds(Action closeCallback = null)
        {
            m_CloseCallback = closeCallback;
            if (Appodeal.isLoaded(Appodeal.INTERSTITIAL))
            {
                Appodeal.show(Appodeal.INTERSTITIAL);
            }
        }

        public void ShowRewardAds(Action closeRewardCallback = null)
        {
            m_CloseRewardCallback = closeRewardCallback;
            if (Appodeal.isLoaded(Appodeal.REWARDED_VIDEO))
            {
                Appodeal.show(Appodeal.REWARDED_VIDEO);
            }
        }

        public void LoadFullAds(Action loadedCallback = null)
        {

        }

        void HandleOnAdClosed(object sender, EventArgs args)
        {
            if (m_CloseCallback != null)
            {
                m_CloseCallback.Invoke();
            }
        }

        public bool IsFullAdsLoaded()
        {
            return Appodeal.isLoaded(Appodeal.INTERSTITIAL);
        }

        public bool IsRewardAdsLoaded()
        {
            return Appodeal.isLoaded(Appodeal.REWARDED_VIDEO);
        }
        public void LoadAndShowBannerBot()
        {

        }

        public void LoadAndShowBannerTop()
        {

        }

        public void LoadBannerBot()
        {

        }

        public void LoadBannerTop()
        {

        }

        public void ShowBanner(bool isRefreshBanner = false)
        {
            isNeedToShowBannerAds = true;

            if (Appodeal.isLoaded(Appodeal.BANNER_BOTTOM))
            {
                Appodeal.show(Appodeal.BANNER_BOTTOM);
            }
        }

        public void HideBanner()
        {
            isNeedToShowBannerAds = false;

            Appodeal.hide(Appodeal.BANNER_BOTTOM);
        }

        public void RefreshBanner()
        {

        }

        public void ShutDown()
        {

        }

        public void onInterstitialLoaded(bool isPrecache) { Debug.Log("Interstitial loaded"); }
        public void onInterstitialFailedToLoad() { Debug.Log("Interstitial failed"); }
        public void onInterstitialShown() { Debug.Log("Interstitial opened"); }
        public void onInterstitialClosed()
        {
            Debug.Log("Interstitial closed");
            if (m_CloseCallback != null)
            {
                m_CloseCallback.Invoke();
            }
        }
        public void onInterstitialClicked() { Debug.Log("Interstitial clicked"); }

        public void onRewardedVideoLoaded() { Debug.Log("Video loaded"); }
        public void onRewardedVideoFailedToLoad() { Debug.Log("Video failed"); }
        public void onRewardedVideoShown() { Debug.Log("Video shown"); }
        public void onRewardedVideoClosed(bool finished)
        {
            if (finished)
            {
                if (m_CloseRewardCallback != null)
                {
                    m_CloseRewardCallback.Invoke();
                }
            }
        }
        public void onRewardedVideoFinished(int amount, string name)
        {
            Debug.Log("Reward: " + amount + " " + name);
        }

        public void onBannerLoaded(bool isPrecache)
        {
            if (isNeedToShowBannerAds)
            {
                Appodeal.show(Appodeal.BANNER_BOTTOM);
            }
        }

        public void onBannerFailedToLoad()
        {

        }

        public void onBannerShown()
        {

        }

        public void onBannerClicked()
        {

        }
    }

#else

    public class AppodealModule : IAdsService
    {
        public void HideBanner()
        {
            throw new NotImplementedException();
        }

        public void Init(AdsConfig adsConfig)
        {
            throw new NotImplementedException();
        }

        public bool IsFullAdsLoaded()
        {
            throw new NotImplementedException();
        }

        public void LoadAndShowBannerBot()
        {
            throw new NotImplementedException();
        }

        public void LoadAndShowBannerTop()
        {
            throw new NotImplementedException();
        }

        public void LoadBannerBot()
        {
            throw new NotImplementedException();
        }

        public void LoadBannerTop()
        {
            throw new NotImplementedException();
        }

        public void LoadFullAds(Action loadedCallback = null)
        {
            throw new NotImplementedException();
        }

        public void RefreshBanner()
        {
            throw new NotImplementedException();
        }

        public void ShowBanner(bool isRefreshBanner = false)
        {
            throw new NotImplementedException();
        }

        public void ShowFullAds(Action closeCallback = null)
        {
            throw new NotImplementedException();
        }

        public bool ShowFullAdsWithTime(float timeCondition, Action closeCallback = null)
        {
            throw new NotImplementedException();
        }

        public void ShutDown()
        {
            throw new NotImplementedException();
        }
    }

#endif
}
