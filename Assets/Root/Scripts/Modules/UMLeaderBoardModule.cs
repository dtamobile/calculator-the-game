﻿using System;

namespace Root
{
	public class UMLeaderBoardModule : ILeaderBoardService
	{
        Action m_Callback;

        public void ConnectService()
        {
            UM_GameServiceManager.Instance.Connect();
        }

        bool IsConnected()
        {
            return UM_GameServiceManager.Instance.IsConnected;
        }

        void ConnectThenRunCallback(Action callback)
        {
            UM_GameServiceManager.Instance.Connect();
            
            UM_GameServiceManager.OnPlayerConnected += OnPlayerConnected;
            m_Callback = callback;
        }

        void OnPlayerConnected()
        {
            UM_GameServiceManager.OnPlayerConnected -= OnPlayerConnected;

            if(m_Callback != null)
            {
                m_Callback();
                m_Callback = null;
            }
        }

        public int GetPlayerScore(string leaderBoardID, int defaultValue) {
			var lb = GetLB (leaderBoardID);
			int serverScore = defaultValue;

			if (lb != null) {
				var um_score = lb.GetCurrentPlayerScore (UM_TimeSpan.ALL_TIME, UM_CollectionType.GLOBAL);

				if (um_score != null) {
					serverScore = (int) um_score.LongScore;
				}
			}
			return serverScore;
		}

        public void ShowLeaderBoard()
        {
            if (IsConnected())
            {
                UM_GameServiceManager.Instance.ShowLeaderBoardsUI();
            }
            else
            {
                ConnectThenRunCallback(() =>
                {
                    UM_GameServiceManager.Instance.ShowLeaderBoardsUI();
                });
            }
        }

        public void SubmitPlayerScore(string leaderBoardID, int value) {
            if (IsConnected())
            {
                UM_GameServiceManager.Instance.SubmitScore(leaderBoardID, value);
            }
            else
            {
                ConnectThenRunCallback(() =>
                {
                    UM_GameServiceManager.Instance.SubmitScore(leaderBoardID, value);
                });
            }
		}

		UM_Leaderboard GetLB (string leaderBoardID) {
			return UM_GameServiceManager.Instance.GetLeaderboard (leaderBoardID);
		}
	}
}

