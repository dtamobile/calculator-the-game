﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Root
{
	public class UMInAppPurchaseModule : IInAppPurchaseService
	{
		IDataService dataService;

        public UMInAppPurchaseModule(IDataService dataService)
        {
            this.dataService = dataService;
        }

		Dictionary<string, Action<bool>> listCallBacks;

		Dictionary<string, Action<bool>> GetListCallBacks () {
			if (listCallBacks == null) {
				listCallBacks = new Dictionary<string, Action<bool>> ();
			}
			return listCallBacks;
		}

        public void ConnectInit()
        {
            if (!UM_InAppPurchaseManager.Client.IsConnected)
            {
                Debug.Log("Init IAP");
                UM_InAppPurchaseManager.Client.OnServiceConnected += OnBillingConnectFinishedAction;
                UM_InAppPurchaseManager.Client.Connect();
            }
        }

        public bool IsProductPurchased(string productID)
        {
            if (!UM_InAppPurchaseManager.Client.IsConnected)
            {
                return dataService.GetBool(productID, false);
            }
            else
            {
                return UM_InAppPurchaseManager.Client.IsProductPurchased(productID);
            }
        }

        public void PurchaseProduct(string productID, Action<bool> callBack)
        {
            if (!UM_InAppPurchaseManager.Client.IsConnected)
            {
                Debug.Log("IAP not init");
                return;
            }

            if (UM_InAppPurchaseManager.Client.IsProductPurchased(productID))
            {
                dataService.SetBool(productID, true);
                callBack(true);
                ShowPopup("Purchases Restored", "Your previously purchased products have been restored.");
            }
            else
            {
                listCallBacks = GetListCallBacks();
                if (listCallBacks.ContainsKey(productID))
                {
                    listCallBacks[productID] = callBack;
                }
                else
                {
                    listCallBacks.Add(productID, callBack);
                }

                ShowPurchasingPreloader();
                UM_InAppPurchaseManager.Client.OnPurchaseFinished += OnPurchaseFlowFinishedAction;
                UM_InAppPurchaseManager.Client.Purchase(productID);
            }
        }

        public void RestorePurchase(Dictionary<string, Action<bool>> callbacks)
        {
            listCallBacks = callbacks;

            Debug.Log("RestoreButtonOnClick");

            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                UM_InAppPurchaseManager.Client.RestorePurchases();
            }
            else if (Application.platform == RuntimePlatform.Android)
            {
                CheckRestoreAndroid();
            }

        }

        private void CheckRestoreAndroid()
        {

            if (UM_InAppPurchaseManager.Client.IsConnected && Application.platform == RuntimePlatform.Android)
            {

                listCallBacks = GetListCallBacks();
                bool isSuccessful = false;

                foreach (var productID in listCallBacks.Keys)
                {
                    if (UM_InAppPurchaseManager.Client.IsProductPurchased(productID))
                    {
                        Debug.Log(productID + " IsProductPurchased");
                        dataService.SetBool(productID, true);
                        isSuccessful = true;
                        listCallBacks[productID].Invoke(true);
                    }
                    else
                    {
                        Debug.Log(productID + " Is Not Purchased");
                        dataService.SetBool(productID, false);
                        listCallBacks[productID].Invoke(false);
                    }
                }
                listCallBacks.Clear();

                if (isSuccessful)
                {
                    ShowPopup("Purchases Restored", "Your previously purchased products have been restored.");
                }
                else
                {
                    ShowPopup("Failed", "There are no items available to restore at this time.");
                }
            }
            else
            {
                ShowPopup("Connection Error!", "Please check your internet connection and try again.");
            }

        }

        private void OnBillingConnectFinishedAction(UM_BillingConnectionResult result)
        {
            UM_InAppPurchaseManager.Client.OnServiceConnected -= OnBillingConnectFinishedAction;
            if (result.isSuccess)
            {
                Debug.Log("Connected UM_InAppPurchaseManager");
            }
            else
            {
                Debug.Log("Failed to connect UM_InAppPurchaseManager");
            }
        }

        private void OnPurchaseFlowFinishedAction(UM_PurchaseResult result)
        {

            if (result.isSuccess)
            {
                Debug.Log("Product " + result.product.id + " purchase Success");
                ShowPopup("Thank you!", "Product " + result.product.id + " purchase Success");

                string productID = result.product.id;
                dataService.SetBool(productID, true);

                listCallBacks = GetListCallBacks();
                if (listCallBacks.ContainsKey(productID))
                {
                    listCallBacks[productID].Invoke(true);
                    listCallBacks.Remove(productID);
                }

            }
            else
            {
                Debug.Log("Product " + result.product.id + " purchase Failed");
                ShowPopup("Fail!", "Product " + result.product.id + " purchase Failed");

                string productID = result.product.id;
                dataService.SetBool(productID, false);

                listCallBacks = GetListCallBacks();
                if (listCallBacks.ContainsKey(productID))
                {
                    listCallBacks[productID].Invoke(false);
                    listCallBacks.Remove(productID);
                }
            }

            HidePurchasingPreloader();
            UM_InAppPurchaseManager.Client.OnPurchaseFinished -= OnPurchaseFlowFinishedAction;
        }
        
        void ShowPurchasingPreloader()
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                MNP.ShowPreloader("Purchasing...", "");
            }
        }

        void HidePurchasingPreloader()
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                MNP.HidePreloader();
            }
        }

        void ShowPopup(string title, string message)
        {
            var popup = new MNPopup(title, message);
            popup.AddAction("OK", null);
            popup.Show();
        }

        void OnRestoreFinished(UM_BaseResult result)
        {
            if (result.IsSucceeded)
            {

            }
        }
    }
}

