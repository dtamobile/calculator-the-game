﻿//need to define if using UNITY_ADS
#define UNITY_ADS_ACTIVE

using System;
using UnityEngine;

#if (UNITY_ADS_ACTIVE)
using UnityEngine.Advertisements;
#endif

namespace Root
{
    public class UnityAdsRewardModule : IAdsRewardService
    {
#if (UNITY_ADS_ACTIVE)
        private Action m_RewardAction;
        private Action m_NotRewardAction;
        const string c_AdsID = "rewardedVideo";

        public void Init()
        {

        }

        public void ShowAds(Action rewardCallback = null, Action notRewardCallback = null)
        {
            if (rewardCallback != null)
            {
                m_RewardAction = rewardCallback;
            }

            if (notRewardCallback != null)
            {
                m_NotRewardAction = notRewardCallback;
            }

            ShowOptions options = new ShowOptions();
            options.resultCallback = HandleShowResult;

            Advertisement.Show(c_AdsID, options);
        }

        void HandleShowResult(ShowResult result)
        {
            if (result == ShowResult.Finished)
            {
                RunCallback(true);
            }
            else if (result == ShowResult.Skipped)
            {
                RunCallback(false);
            }
            else if (result == ShowResult.Failed)
            {
                RunCallback(false);
            }
        }

        void RunCallback(bool isReward)
        {
            if (isReward)
            {
                if (m_RewardAction != null)
                {
                    m_RewardAction();
                    m_RewardAction = null;
                }
            }
            else
            {
                if (m_NotRewardAction != null)
                {
                    m_NotRewardAction();
                    m_NotRewardAction = null;
                }
            }

        }

        public bool IsAdsAvailable()
        {
            return Advertisement.IsReady(c_AdsID);
        }
#else
        public void Init()
        {
            throw new NotImplementedException();
        }

        public bool IsAdsAvailable()
        {
            throw new NotImplementedException();
        }

        public void ShowAds(Action rewardCallback = null, Action notRewardCallback = null)
        {
            throw new NotImplementedException();
        }
#endif
    }
}

