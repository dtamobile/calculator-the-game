﻿using System;
using System.Collections.Generic;

namespace Root
{
	public class LeaderBoardMockModule : ILeaderBoardService
	{
		Dictionary<string, int> data;

        public void ConnectService()
        {

        }

        public int GetPlayerScore(string leaderBoardID, int defaultValue) {
			var dict = GetData ();
			if (dict.ContainsKey (leaderBoardID)) {
				return dict [leaderBoardID];
			}
			return defaultValue;
		}

        public void ShowLeaderBoard()
        {

        }

        public void SubmitPlayerScore(string leaderBoardID, int value) {
			var dict = GetData ();
			if (dict.ContainsKey (leaderBoardID)) {
				dict [leaderBoardID] = value;
			} else {
				dict.Add (leaderBoardID, value);
			}
		}

		Dictionary<string, int> GetData() {
			if (data == null) {
				data = new Dictionary<string, int> ();
			}
			return data;
		}
	}
}

