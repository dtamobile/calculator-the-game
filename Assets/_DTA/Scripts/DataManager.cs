﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : Singleton<DataManager>
{

    public bool m_IsNoAds;
    public void LoadData()
    {
        m_IsNoAds = ServicesManager.DataSecure().GetBool("IsNoAds", false);
    }

    public void SaveData()
    {
        ServicesManager.DataSecure().SetBool("IsNoAds", m_IsNoAds);
    }

    
}
