﻿using System;
using UnityEngine;

public class TutorialLevelData : ScriptableObject {

    public TutorialStep[] steps;
	
}

[Serializable]
public class TutorialStep
{
    public Texture2D faceImg;
    public string faceText;
    public string okButtonText;
    public bool isYesNo = false;
    public bool isGoalTxt = false;
    public bool isMoveTxt = false;
    public bool isHint = false;  
    public string buttonOperator;
    public Color buttonOpeColor;
    public Color buttonOpeShadowColor;
}