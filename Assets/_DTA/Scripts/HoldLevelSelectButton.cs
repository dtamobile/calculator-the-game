﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldLevelSelectButton : Singleton<HoldLevelSelectButton>
{
    float StartHoldTime;
    float HoldTime;
    bool IsPressPrevious = false;
    bool IsPressNext = false;
    // Use this for initialization
    void Start()
    {
        StartHoldTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (IsPressNext && Time.time > StartHoldTime + 0.2f)
        {
            UIManager.Instance.NextLevelBtn();
            StartHoldTime = Time.time;
        }
        if (IsPressPrevious && Time.time > StartHoldTime + 0.2f)
        {
            UIManager.Instance.PreviousLevelBtn();
            StartHoldTime = Time.time;
        }
    }

    public void HoldPrevious()
    {
        StartHoldTime = Time.time;
        IsPressPrevious = true;
    }
    public void HoldNext()
    {
        StartHoldTime = Time.time;
        IsPressNext = true;
    }

    public void NotClickNext()
    {
        StartHoldTime = 0f;
        IsPressNext = false;
    }

    public void NotClickPrevious()
    {
        StartHoldTime = 0f;
        IsPressPrevious = false;
    }
}
