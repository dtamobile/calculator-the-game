﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using System;

public class Analytics : Singleton<Analytics>
{
    Dictionary<string, object> param = new Dictionary<string, object>();

    private void Awake()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            //Handle FB.Init
            FB.Init(() =>
            {
                FB.ActivateApp();
            });
        }
    }

    void PushEvent(string key, float value=1, Dictionary<string, object> parameter = null)
    {
        FB.LogAppEvent(key, value, parameter);
    }

    public void PushEventShowFullAds()
    {
        PushEvent("Show Full Ads");
    }

    public void PushEventRequestAds()
    {
        PushEvent("Request Ads");
    }

    public void PushEventPlay(int level)
    {
        param.Clear();
        param["Level"] = level;
        string eventPlay = "";
        if (level < 11)
        {
            eventPlay = "LevelPlay 1_10";
        }
        else if (level < 21)
        {
            eventPlay = "LevelPlay 11_20";
        }
        else if (level < 31)
        {
            eventPlay = "LevelPlay 21_30";
        }
        else if (level < 41)
        {
            eventPlay = "LevelPlay 31_40";
        }
        else if (level < 51)
        {
            eventPlay = "LevelPlay 41_50";
        }
        else if (level < 61)
        {
            eventPlay = "LevelPlay 51_60";

        }
        else if (level < 71)
        {
            eventPlay = "LevelPlay 61_70";
        }

        else if (level < 81)
        {
            eventPlay = "LevelPlay 71_80";
        }

        else if (level < 91)
        {
            eventPlay = "LevelPlay 81_90";
        }

        else if (level < 101)
        {
            eventPlay = "LevelPlay 91_100";
        }

        else if (level < 111)
        {
            eventPlay = "LevelPlay 101_110";
        }

        else if (level < 121)
        {
            eventPlay = "LevelPlay 111_120";
        }

        else if (level < 131)
        {
            eventPlay = "LevelPlay 121_130";
        }
        PushEvent(eventPlay, 1, param);
    }

    public void PushEventDead(int level)
    {
        param.Clear();
        param["Level"] = level;
        string eventDead = "";
        if (level < 11)
        {
            eventDead = "LevelDead 1_10";
        }
        else if (level < 21)
        {
            eventDead = "LevelDead 11_20";
        }
        else if (level < 31)
        {
            eventDead = "LevelDead 21_30";
        }
        else if (level < 41)
        {
            eventDead = "LevelDead 31_40";
        }
        else if (level < 51)
        {
            eventDead = "LevelDead 41_50";
        }
        else if (level < 61)
        {
            eventDead = "LevelDead 51_60";

        }
        else if (level < 71)
        {
            eventDead = "LevelDead 61_70";
        }

        else if (level < 81)
        {
            eventDead = "LevelDead 71_80";
        }

        else if (level < 91)
        {
            eventDead = "LevelDead 81_90";
        }

        else if (level < 101)
        {
            eventDead = "LevelDead 91_100";
        }

        else if (level < 111)
        {
            eventDead = "LevelDead 101_110";
        }

        else if (level < 121)
        {
            eventDead = "LevelDead 111_120";
        }

        else if (level < 131)
        {
            eventDead = "LevelDead 121_130";
        }
        PushEvent(eventDead, 1, param);
    }

    public void PushEventUseHint(int level)
    {
        param.Clear();
        param["Level"] = level;
        string eventUseHint = "";
        if (level < 11)
        {
            eventUseHint = "LevelUseHint 1_10";
        }
        else if (level < 21)
        {
            eventUseHint = "LevelUseHint 11_20";
        }
        else if (level < 31)
        {
            eventUseHint = "LevelUseHint 21_30";
        }
        else if (level < 41)
        {
            eventUseHint = "LevelUseHint 31_40";
        }
        else if (level < 51)
        {
            eventUseHint = "LevelUseHint 41_50";
        }
        else if (level < 61)
        {
            eventUseHint = "LevelUseHint 51_60";

        }
        else if (level < 71)
        {
            eventUseHint = "LevelUseHint 61_70";
        }

        else if (level < 81)
        {
            eventUseHint = "LevelUseHint 71_80";
        }

        else if (level < 91)
        {
            eventUseHint = "LevelUseHint 81_90";
        }

        else if (level < 101)
        {
            eventUseHint = "LevelUseHint 91_100";
        }

        else if (level < 111)
        {
            eventUseHint = "LevelUseHint 101_110";
        }

        else if (level < 121)
        {
            eventUseHint = "LevelUseHint 111_120";
        }

        else if (level < 131)
        {
            eventUseHint = "LevelUseHint 121_130";
        }
        PushEvent(eventUseHint, 1, param);
    }

    public void PushEventDailyHint(int level)
    {
        param.Clear();
        param["Level"] = level;
        string eventDailyHint = "";
        if (level < 11)
        {
            eventDailyHint = "LevelDailyHint 1_10";
        }
        else if (level < 21)
        {
            eventDailyHint = "LevelDailyHint 11_20";
        }
        else if (level < 31)
        {
            eventDailyHint = "LevelDailyHint 21_30";
        }
        else if (level < 41)
        {
            eventDailyHint = "LevelDailyHint 31_40";
        }
        else if (level < 51)
        {
            eventDailyHint = "LevelDailyHint 41_50";
        }
        else if (level < 61)
        {
            eventDailyHint = "LevelDailyHint 51_60";

        }
        else if (level < 71)
        {
            eventDailyHint = "LevelDailyHint 61_70";
        }

        else if (level < 81)
        {
            eventDailyHint = "LevelDailyHint 71_80";
        }

        else if (level < 91)
        {
            eventDailyHint = "LevelDailyHint 81_90";
        }

        else if (level < 101)
        {
            eventDailyHint = "LevelDailyHint 91_100";
        }

        else if (level < 111)
        {
            eventDailyHint = "LevelDailyHint 101_110";
        }

        else if (level < 121)
        {
            eventDailyHint = "LevelDailyHint 111_120";
        }

        else if (level < 131)
        {
            eventDailyHint = "LevelDailyHint 121_130";
        }
        PushEvent(eventDailyHint, 1, param);
    }
}
