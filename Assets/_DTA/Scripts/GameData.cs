﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameData", menuName = "GameConfig/GameData")]
public class GameData : ScriptableObject {
    [SerializeField]
    bool m_IsNoAds;
}
