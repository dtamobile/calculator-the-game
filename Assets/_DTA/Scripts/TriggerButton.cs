﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TriggerButton : MonoBehaviour
{

    RectTransform ObjectRect;
    Vector3 UpPos;
    Vector3 DownPos;
    // Use this for initialization
    void Awake()
    {
        ObjectRect = gameObject.GetComponent<RectTransform>();
        UpPos = ObjectRect.anchoredPosition;
        DownPos = UpPos - new Vector3(0f, 11f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerUpBtn()
    {
        var shadow = gameObject.GetComponent<Shadow>();
        shadow.effectDistance = new Vector2(0f, -12f);
        ObjectRect.anchoredPosition = UpPos;
    }

    public void OnTriggerDownBtn()
    {
        var shadow = gameObject.GetComponent<Shadow>();
        shadow.effectDistance = new Vector2(0f, -1f);
        ObjectRect.anchoredPosition = DownPos;
    }
}
