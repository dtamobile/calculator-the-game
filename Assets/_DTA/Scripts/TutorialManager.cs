﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;
public class TutorialManager : Singleton<TutorialManager>
{

    public static TutorialLevelData LoadData(int level)
    {
        var data = Resources.Load<TutorialLevelData>("TutLevel_" + level);
        UIManager.Instance.SetTutorialText("LEVEL: " + level);
        return data;

    }

    public int currentLevel;

    TutorialLevelData m_CurrentLevelData;
    int m_CurrentStepID;

    public Button okBtn;
    public Text okBtnTxt;
    public Button noBtn;
    public GameObject Operator_2;
    public GameObject GoalTxt;
    public GameObject MoveTxt;
    public GameObject[] hintBtn;
    public Sprite[] btnOpeSprite;

    public Sprite m_OkBtnUp;
    public Sprite m_OkBtnDown;
    public Sprite m_NoBtnUp;
    public Sprite m_NoBtnDown;
    public Sprite m_HiddenBtn;
    public Sprite m_RedBtn;

    Vector3 okUpPos;
    Vector3 okDownPos;
    Vector3 NoUpPos;
    Vector3 NoDownPos;
    Vector3 btnOpeRectPos;
    Vector3 btnOpeRectUpPos;
    RectTransform btnOpeRect;


    public GameObject btnOpe;
    public RawImage face;
    public Text faceText;
    public Text buttonOperator;

    private void Awake()
    {
        btnOpeRect = btnOpe.GetComponent<RectTransform>();
        btnOpeRectPos = btnOpeRect.anchoredPosition;
        btnOpeRectUpPos = btnOpeRectPos + new Vector3(0f, 12f);
    }
    public bool InitTut(int level)
    {
        var tutData = TutorialManager.LoadData(level);
        if (tutData == null)
        {
            return false;
        }
        else
        {
            m_CurrentLevelData = tutData;
            return true;
        }
    }

    private void Start()
    {
        okUpPos = okBtn.transform.position;
        okDownPos = okUpPos - new Vector3(0f, 8f);
        NoUpPos = noBtn.transform.position;
        NoDownPos = NoUpPos - new Vector3(0f, 4f);

    }

    public void ShowTut()
    {
        m_CurrentStepID = 0;
        //BlurAll();
        ShowTut(m_CurrentLevelData.steps[m_CurrentStepID]);
    }

    void ShowTut(TutorialStep step)
    {
        var btnOpeImage = btnOpe.GetComponent<Image>();
        var btnOpeShadow = btnOpe.GetComponent<Shadow>();
        faceText.gameObject.SetActive(true);
        face.gameObject.SetActive(true);
        okBtn.gameObject.SetActive(true);
        okBtnTxt.gameObject.SetActive(true);
        
        for(int i=0; i<hintBtn.Length; i++)
        {
            if(!step.isHint)
            {
                hintBtn[i].SetActive(false);
            }
            else
            {
                hintBtn[i].SetActive(true);
            }
        }

        if (!step.isGoalTxt)
        {
            GoalTxt.SetActive(false);
        }
        else
        {
            GoalTxt.SetActive(true);
        }

        if (step.isMoveTxt)
        {
            MoveTxt.SetActive(true);
        }
        else
        {
            MoveTxt.SetActive(false);
        }

        if (step.faceImg != null)
        {
            face.texture = step.faceImg;
        }
        faceText.text = step.faceText;
        if (step.faceText.Length <= 4)
        {
            faceText.fontSize = 210;
        }
        else if (step.faceText.Length <= 10)
        {
            faceText.fontSize = 130;
        }
        else if (step.faceText.Length <= 20)
        {
            faceText.fontSize = 105;
        }
        else if(step.faceText.Length <=50)
        {
            faceText.fontSize = 65;
        }
        else
        {
            faceText.fontSize = 62;
        }
        
        var noBtnScript = noBtn.GetComponent<Button>();
        var noBtnTrigger = noBtn.GetComponent<EventTrigger>();
        if (step.isYesNo)
        {
            noBtn.gameObject.SetActive(true);
            Operator_2.SetActive(false);
            noBtnScript.enabled = true;
            noBtnTrigger.enabled = true;
        }
        else
        {
            noBtn.gameObject.SetActive(false);
            Operator_2.SetActive(true);
            noBtnScript.enabled = false;
            noBtnTrigger.enabled = false;
        }
        okBtnTxt.text = step.okButtonText;
        
        if(string.IsNullOrEmpty(step.buttonOperator))
        {
            buttonOperator.gameObject.SetActive(false);
            btnOpeImage.color = Color.white;
            btnOpeShadow.enabled = false;
            btnOpeImage.sprite = btnOpeSprite[0];
            btnOpeRect.anchoredPosition = btnOpeRectPos;
        }
        else
        {
            buttonOperator.gameObject.SetActive(true);
            buttonOperator.text = step.buttonOperator;
            btnOpeImage.color = step.buttonOpeColor;
            btnOpeShadow.enabled = true;
            btnOpeShadow.effectColor = step.buttonOpeShadowColor;
            btnOpeImage.sprite = btnOpeSprite[1];
            btnOpeRect.anchoredPosition = btnOpeRectUpPos;
        }
        
        if(!string.IsNullOrEmpty(step.buttonOperator.Trim()))
        {
            if (step.buttonOperator.Length <= 4)
            {
                buttonOperator.fontSize = 50;
            }
            else
            {
                buttonOperator.fontSize = 32;
            }
        }
    }

    public void OnClickOK()
    {
        Debug.Log("Click OK");
        m_CurrentStepID += 1;
        if (m_CurrentStepID >= m_CurrentLevelData.steps.Length)
        {
            //end tutorial
            GameManager.Instance.OnTutorialDone();
        }
        else
        {
            ShowTut(m_CurrentLevelData.steps[m_CurrentStepID]);
        }
        UIManager.Instance.PlayButtonSound();
    }

    public void OnClickNo()
    {
        m_CurrentStepID = 0;
        ShowTut(m_CurrentLevelData.steps[m_CurrentStepID]);
        UIManager.Instance.PlayButtonSound();
    }

    public void OnTriggerDownOKBtn()
    {
        Debug.Log("Trigger Down");
        //var image = okBtn.GetComponent<Image>();
        //image.sprite = m_OkBtnDown;
        var shadow = okBtn.GetComponent<Shadow>();
        shadow.enabled = false;
        var currentPos = new Vector3();
        currentPos = okDownPos;
        okBtn.transform.position = currentPos;
    }

    public void OnTriggerUpOKBtn()
    {
        Debug.Log("Trigger Up");
        //    var image = okBtn.GetComponent<Image>();
        //    image.sprite = m_OkBtnUp;
        var shadow = okBtn.GetComponent<Shadow>();
        shadow.enabled = true;
        var currentPos = new Vector3();
        currentPos = okUpPos;
        okBtn.transform.position = currentPos;
    }

    public void OnTriggerUpNoBtn()
    {
        var shadow = noBtn.GetComponent<Shadow>();
        shadow.enabled = true;
        var currentPos = new Vector3();
        currentPos = NoUpPos;
        noBtn.transform.position = currentPos;
    }

    public void OnTriggerDownNoBtn()
    {
        var shadow = noBtn.GetComponent<Shadow>();
        shadow.enabled = false;
        var currentPos = new Vector3();
        currentPos = NoDownPos;
        noBtn.transform.position = currentPos;
    }
}
