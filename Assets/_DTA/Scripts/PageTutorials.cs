﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Tut
{
    public int ID;
    public List<PageTutorials> Tutorial = new List<PageTutorials>();
}

[Serializable]
public class PageTutorials : MonoBehaviour {

    public ButtonTut m_ConfirmBtn;
    public ButtonTut m_RejectBtn;
    public ButtonTut m_NewFuncBtn;
    public Sprite m_ImageFace;
    public ArrowTut m_Arrow;
    public string m_Content;
}
[Serializable]
public class ButtonTut:MonoBehaviour
{
    public int index;
    public string textButton;
    public Sprite imageButton;
    


}

[Serializable]
public class ArrowTut:MonoBehaviour
{
    public Vector3 m_position;
    public GameObject m_ThisObject;
}